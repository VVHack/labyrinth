﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class goBackSettings : MonoBehaviour {
	GameObject settingsObject,board,SubjectInfo, MenuObject;
	public Slider sensSlider;
	public InputField sensInputField;
	public GameObject background;
	public Text sensWarningText;

	public Slider angleSlider;
	public InputField angleInputField;
	public Text angleWarningText;




	int count = 1;
	// Use this for initialization
	void Start () {
		settingsObject = GameObject.Find ("SettingsObject");
		board = settingsObject.GetComponent<LoadSettingsScript> ().board;
		SubjectInfo = settingsObject.GetComponent<LoadSettingsScript> ().SubjectInfo;
		MenuObject = settingsObject.GetComponent<LoadSettingsScript> ().MenuObject;
	}
	
	// Update is called once per frame
	void Update () {
		if (count++ == 1) {
			sensSlider.value = MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity;
			angleSlider.value = (float)board.GetComponent<BoardController> ().maxAngle;
		}
	}

	public void onSensitivityEntered(string sens){
		sensWarningText.gameObject.SetActive (false);
		try{
			double s = Convert.ToDouble(sens);
			if(s>100*sensSlider.maxValue){
				sensInputField.text = (100*sensSlider.maxValue).ToString();
				sensSlider.value = sensSlider.maxValue;
			}
			else if(s<0){
				sensInputField.text = "0";
				sensSlider.value = 0;
			}
			else{
				sensSlider.value = (float)s/100;
			}
		}
		catch(FormatException e){
			sensWarningText.gameObject.SetActive (true);
		}
	}

	public void onAngleEntered(string angle){
		angleWarningText.gameObject.SetActive (false);
		try{
			double a = Convert.ToDouble(angle);
			if(a>angleSlider.maxValue){
				angleInputField.text = (angleSlider.maxValue).ToString();
				angleSlider.value = angleSlider.maxValue;
			}
			else if(a<0){
				angleInputField.text = "0";
				angleSlider.value = 0;
			}
			else{
				angleSlider.value = (float)a;
			}
		}
		catch(FormatException e){
			angleWarningText.gameObject.SetActive (true);
		}
	}

	public void goBack(){
		GameObject Camera = GameObject.Find ("Main Camera");
		GameObject Light = GameObject.Find ("Directional Light");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject EventSystem = GameObject.Find ("EventSystem");
		GameObject Background = GameObject.Find ("Background");

		Destroy (Camera);
		Destroy (Light);
		Destroy (Canvas);
		Destroy (EventSystem);
		Destroy (background);

		settingsObject.GetComponent<LoadSettingsScript> ().LoadMenu ();
	}

	public void changeSensitivity(float sensitivity){
		MenuObject.GetComponent<MenuScript> ().changeSensitivity (sensitivity);
		sensInputField.text = (100*sensSlider.value).ToString();
	}
	public void changeMaxAngle(float angle){ //added to a slider
		MenuObject.GetComponent<MenuScript> ().changeMaxAngle (angle);
		angleInputField.text = angle.ToString();
	}

}
