﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class PlotStatsScript : MonoBehaviour {
	MenuControllerObject menuController;
	public GameObject MenuObject,board,Player,SubjectInfo;
	// Use this for initialization
	void Start () {
		menuController = new MenuControllerObject (gameObject,gameObject.name,7);
		MenuObject = GameObject.Find ("MenuObject");
		board = MenuObject.GetComponent<MenuScript> ().board;
		Player = MenuObject.GetComponent<MenuScript> ().Player;
		SubjectInfo = MenuObject.GetComponent<MenuScript> ().SubjectInfo;
		DontDestroyOnLoad (gameObject);//must be destroyed when going back from the parent menu scene
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadMenu(){
		menuController.LoadMenu ();
	}

	void OnApplicationQuit(){
		try{ 
			if (menuController.menuScene) { //needs to work only if it is MenuScene because player is deactivated

				UnityEngine.Debug.Log ("PlotStatsScript.cs OnApplicationQuit()");
				if(MenuObject.GetComponent<MenuScript>().playing && SubjectInfo.GetComponent<InfoScript>().menuGameCount!=1)Player.GetComponent<BallControler>().throwAwayAttemptFiles();
				if(File.Exists("record.txt"))File.Delete ("record.txt"); //we have to delete this file any way
			}}
		catch(ArgumentException e){
		}
	}
}
