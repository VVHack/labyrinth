﻿using UnityEngine;
using System.Collections;

public class DeactivateScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	//The menu script deletes any inactive objects without a parent while resuming the game which might be dangerous if that object is needed in future in whuch case 
	//this function must be used which makes it a child of this always active empty gameobject which saves the empty game object

	public void deactivate(GameObject go){
		if (go.transform.parent == null ) {
			go.transform.SetParent (transform);
			go.SetActive (false);
		} else {
			go.SetActive (false);
		}
	}

	public void activate(GameObject go){
		if (go.transform.parent != transform) {
			go.SetActive (true);
		} else {
			go.SetActive (true);
			go.transform.SetParent (null);
		}
	}
//This is how to use a delegate to access a private function
//	using System;
//	using System.IO;
//	
//	namespace DelegateAppl
//	{
//		class PrintString
//		{
//			class func{
//				static private int f(int i){
//					return i+1;
//				}
//				public delegate int del(int i);
//				public del d = new del(f);
//				
//			}
//			static void Main(){
//				Console.WriteLine("Hello World!");
//				func Func = new func();
//				Console.WriteLine(Func.d(1));
//			}
//		}

}
