﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;

//the function of an object that contains a MenuControllerObject is to maintain the transition between
//the target menu  scene and back and also to transport references to the main gameobjects for 
//object to use in the target menu scene
//There must be a Go Back button and a custom goBack() function for every MenuControllerObject

public class MenuControllerObject{
	public bool menuScene = false;
	GameObject[] objects;
	public int sceneToLoad;
	public string buttonName;
	public GameObject buttonObject;


	/// <summary>
	/// EFFECTS: Creates an instance of this object, takes in the name of the GameObject that contains it, 
	///          the name of the GameObject and the scene it should load when the user wants to access the menu
	/// </summary>
	/// <param name="buttonObject">Button object.</param>
	/// <param name="name">Name.</param>
	/// <param name="scene">Scene.</param>
	public MenuControllerObject(GameObject buttonObject,string name,int scene){
		sceneToLoad = scene;
		buttonName = name;
		this.buttonObject = buttonObject;
		loadObjects ();
	}

	void loadObjects(){
		objects = UnityEngine.Object.FindObjectsOfType<GameObject> ();
	}

	public void LoadMenu(){
		if (!menuScene) {
			menuScene = true;
			foreach (object go in objects) {
				GameObject g = (GameObject)go;
				if (g.transform.parent == null) {
					g.transform.SetParent (buttonObject.transform);
					if (g.name != buttonName)
						g.SetActive (false);
				}
			}
			SceneManager.LoadScene (sceneToLoad);
		} else if (menuScene) {
			menuScene = false;
			foreach (object go in objects) {
				GameObject g = (GameObject)go;
				if (g.transform.parent == buttonObject.transform) {
					g.SetActive (true);
					g.transform.SetParent (null);
				}
			}
		}
	}


}
