﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using System.IO.Ports;


public class GoBack : MonoBehaviour {
	GameObject MenuObject;
	public Text t;
	public string Question="";
	string ID="";
	public GameObject ChangeMazeButton;
	int activeMaze;
	public GameObject SubjectInfo;
	public GameObject board;
	public string address="";
	public string addressQuestion="";
	public string folder_question="";
	public Dropdown FirstJointMenu,SecondJointMenu;
	int chosen_value_1,chosen_value_2;
	int toggle_count=0;
	public Text Knee,Hip,Ankle,Wrist,Elbow;
	public Text KeyText;
	public Dropdown changeMazeDropdown;
	int bendingSense_1 = 1,bendingSense_2 = 1;
	public SerialPort ardPort;
	public Text BackText;
	int dialogPosCorrection = 300,dialogPosCorrectionX=10,space = 80;
	public GameObject SensitivityObject,MaxAngleObject;
	float sizeCorrection = 0.0012f;
	float scaleGUI = 1f;
	public GUIStyle style;
	public GameObject SubjectIDText;
	public GameObject AddressQuestionText;
	public GameObject FolderQuestionText;
	string originalPath;
	public bool flexMode = true;
	public GameObject GameCalibrationButton, SensorCalibrationButton, CalibrationButton, zeroGonButton, zeroFlexButton;
	public GameObject ModeIndicator;
	public InputField nameField, fileAddressField, addressField, SensField;
	public Text sensWarningText;
	public InputField SerialPortField;
	//public bool ardPortSuccess = true;

	int count = 1;


	// Use this for initialization
	void Start () {
		MenuObject = GameObject.Find ("MenuObject");
		activeMaze = MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().activeMaze;
		try{
		ChangeMazeButton = GameObject.Find ("ChangeMaze");
		ChangeMazeButton.GetComponentInChildren<Text> ().text = "Assigned Maze: " + activeMaze.ToString ();
		}
		catch(NullReferenceException e){};
		SubjectInfo = MenuObject.GetComponent<MenuScript> ().SubjectInfo;
		board = MenuObject.GetComponent<MenuScript> ().board;



		try{
		ardPort = new SerialPort(board.GetComponent<ArdController>().portString);
		ardPort.Open ();
		}
		catch(IOException e){
			//ardPortSuccess = false;
		}




		FirstJointMenu = GameObject.Find ("FirstJointMenu").GetComponent<Dropdown>();
		SecondJointMenu = GameObject.Find ("SecondJointMenu").GetComponent<Dropdown>();
		SubjectInfo.GetComponent<InfoScript> ().Dof1 = FirstJointMenu.captionText.text;
		SubjectInfo.GetComponent<InfoScript> ().Dof2 = SecondJointMenu.captionText.text;
		FirstJointMenu.onValueChanged.AddListener (
			delegate {
			log_joint_1 (FirstJointMenu);
		}
		
		);
		SecondJointMenu.onValueChanged.AddListener (
			delegate {
			log_joint_2 (SecondJointMenu);
		}
		
		);


		UnityEngine.Debug.Log ("keyBoardUse: " + MenuObject.GetComponent<MenuScript>().board.GetComponent<BoardController>().keyBoardUse);

		KeyText = GameObject.Find ("KeyText").GetComponent<Text>();
		if (MenuObject.GetComponent<MenuScript>().board.GetComponent<BoardController>().keyBoardUse) {
			KeyText.text = "Keyboard Mode: On";
		} else {
			KeyText.text = "Keyboard Mode: Off";
		}

//		if (ardPortSuccess) {
//			if (MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().keyBoardUse) {
//				if(MenuObject.GetComponent<MenuScript>().menuAttemptCount==1)keyBoardToggle ();
//			}
//
//		}

		changeMazeDropdown = GameObject.Find ("ChangeMazeDropdown").GetComponent<Dropdown>();
		changeMazeDropdown.onValueChanged.AddListener (
			delegate{
				changeMazeFromDropdown(changeMazeDropdown);
			}
		);
		changeMazeDropdown.value = MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().activeMaze-1;
		if (SubjectInfo.GetComponent<InfoScript> ().menuGameCount == 1) {
			BackText = GameObject.Find ("BackText").GetComponent<Text>();
			BackText.text = "Start Game!";
		}
		//SubjectIDText.SetActive(true);
		originalPath = SubjectInfo.GetComponent<InfoScript>().folder_path;
		ModeIndicator = GameObject.Find ("ModeIndicator");
		nameField = GameObject.Find ("NameField").GetComponent<InputField>();
		initNameField ();
		if (!flexMode) {
			fileAddressField = GameObject.Find ("FileAddressField").GetComponent<InputField> ();
			initFileAddressField ();
		}
		addressField = GameObject.Find ("AddressField").GetComponent<InputField>();
		initAddressField ();

		SerialPortField = GameObject.Find ("SerialPortField").GetComponent<InputField> ();
		initSerialPortField ();

		//AutodetectArduinoPort ();
	}
	
	
	void log_joint_1(Dropdown d){
		UnityEngine.Debug.Log (d.captionText.text);
		MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().chosen_value_1 = d.value;
		SubjectInfo.GetComponent<InfoScript> ().Dof1 = d.captionText.text;
	}
	void log_joint_2(Dropdown d){
		UnityEngine.Debug.Log (d.captionText.text);
		MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().chosen_value_2 = d.value;
		SubjectInfo.GetComponent<InfoScript> ().Dof2 = d.captionText.text;
	}

	void changeMazeFromDropdown(Dropdown d){
		string selectedOption = changeMazeDropdown.captionText.text;
		int mazeToActivate = d.value + 1;
		MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().activeMaze = mazeToActivate;
	}

	// Update is called once per frame
	void Update () {
		SubjectIDText = GameObject.Find ("SubjectIDText");
		FirstJointMenu.value = MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().chosen_value_1;
		SecondJointMenu.value = MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().chosen_value_2;
		ID = SubjectInfo.GetComponent<InfoScript> ().Name;
		address = SubjectInfo.GetComponent<InfoScript> ().voltAddress;

		getResistanceFromMenu ();

	
	}

	//EFFECTS: Clears everything from the menu scene and calls the LoadMenu function from MenuScript to recreate the game
	//scene
	public void goBack(){
		GameObject camera = GameObject.Find ("Main Camera");
		GameObject light = GameObject.Find ("Directional Light");
		GameObject EventSystem = GameObject.Find ("EventSystem");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject Background = GameObject.Find ("Background");
		//Destroy (camera);
		Destroy (Background);
		Destroy (camera);
		Destroy (light);
		Destroy (EventSystem);
		Destroy (Canvas);//All buttons and texts would be destroyed with this
		MenuObject.GetComponent<MenuScript> ().LoadMenu ();


		Destroy (gameObject); //although this gets destroyed along with the Canvas object

	}

	public void zeroGoniometers(){
		Stream stream = File.Open (@address, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //@"C:\Users\PM&R\Desktop\data.txt"
		StreamReader streamReader = new StreamReader (stream);
		string inputString = streamReader.ReadLine ();
		
		//all the necessary calibration data is stored in SubjectInfo as well so 
	    //that it can be retrieved on each attempt

		String [] numStrings = new String[2];
		String string1, string2;
		splitString (inputString, numStrings);
		board.GetComponent<GonControllerScript> ().kneeZero = Convert.ToDouble(numStrings[0]);
		board.GetComponent<GonControllerScript> ().anat_zero_1 = Convert.ToDouble(numStrings[0]);
		board.GetComponent<GonControllerScript> ().Dof1=board.GetComponent<GonControllerScript> ().anat_zero_1;
		SubjectInfo.GetComponent<InfoScript>().anat_zero_1=board.GetComponent<GonControllerScript> ().anat_zero_1;
		board.GetComponent<GonControllerScript> ().hipZero = Convert.ToDouble(numStrings[1]);
		board.GetComponent<GonControllerScript> ().anat_zero_2 = Convert.ToDouble(numStrings[1]);
		board.GetComponent<GonControllerScript> ().Dof2=board.GetComponent<GonControllerScript> ().anat_zero_2;
		SubjectInfo.GetComponent<InfoScript>().anat_zero_2=board.GetComponent<GonControllerScript> ().anat_zero_2;
		SubjectInfo.GetComponent<InfoScript>().kneeZero=board.GetComponent<GonControllerScript> ().kneeZero;
		SubjectInfo.GetComponent<InfoScript>().hipZero =board.GetComponent<GonControllerScript> ().hipZero; 
	}

	//EFFECTS: Reverses the boolean variable keyBoardUse in BoardController so that the board may take data from the 
	//keyboard or from the external device(goniometer or bend sensor) accordingly.  It resets the positions of the board
	//and the player, changes the text on the button according to the current mode.  If keyboard is switched off
	//, the function gets the straight resistances from the bend sensors and starts serial communication with the arduino
	public void keyBoardToggle (){
		UnityEngine.Debug.Log ("Keyboard toggle called");
		reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<BoardController>().keyBoardUse);
		//reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().keyboard);
		MenuObject.GetComponent<MenuScript> ().board.GetComponent<ArdController> ().setBoardRotation ();
		MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().resetPosition ();
		if (MenuObject.GetComponent<MenuScript>().board.GetComponent<BoardController>().keyBoardUse) {
			KeyText.text = "Keyboard Mode: On";
//			MenuObject.GetComponent<MenuScript> ().board.GetComponent<GonControllerScript> ().TextAngD1.text = "";
//			MenuObject.GetComponent<MenuScript> ().board.GetComponent<GonControllerScript> ().TextAngD2.text = "";
//			MenuObject.GetComponent<MenuScript> ().board.GetComponent<GonControllerScript> ().NewAng1.text = "";
//			MenuObject.GetComponent<MenuScript> ().board.GetComponent<GonControllerScript> ().NewAng2.text = "";
		} else {
			KeyText.text = "Keyboard Mode: Off";
//			//Serial Communication should only be started if the keyboard mode is turned off for the first time
//			//in the first attempt
//			//Some sort of a makeshift calibration has been done here which should also be done at the same time
//			if (toggle_count == 0) {
//				if (SubjectInfo.GetComponent<InfoScript> ().attempts == 0) {
//					board.GetComponent<ArdController> ().SubjectInfo = SubjectInfo;
//					board.GetComponent<ArdController> ().startSerialComm ();
//					board.GetComponent<ArdController> ().getStraightResistances ();
//				}
//				//this must be done for each attempt whener the first time the keyboard mode is turned off for that
//				//particular attempt
//				board.GetComponent<ArdController> ().initializePlotStream ();
//			}
		}
		//toggle_count is stored in SubjectInfo so that we know whether the keyboard mode has been turned off before
		//in the whole game or not
		++SubjectInfo.GetComponent<InfoScript> ().toggle_count;
		++MenuObject.GetComponent<MenuScript> ().SubjectInfo.GetComponent<InfoScript>().mode_count;



	}

	//EFFECTS: Loads the menu for entering information.  Done in this script as this is one of the scripts that gets activated when the menu scene loads
	void OnGUI(){
		GUI.skin.textField.fontSize = 10 * (int)Math.Ceiling(Screen.width * sizeCorrection);
		Question = "Subject ID";
		//GUI.Label (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (250+dialogPosCorrection)*Screen.height*sizeCorrection, 1000*Screen.width*sizeCorrection, 1000*Screen.height*sizeCorrection), Question, style);

		//SubjectIDText.transform.position = new Vector3((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (250+dialogPosCorrection)*Screen.height*sizeCorrection,0);
//		ID= GUI.TextField (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (280+dialogPosCorrection)*Screen.height*sizeCorrection, 300*sizeCorrection*Screen.width*scaleGUI, 30*sizeCorrection*Screen.height*scaleGUI),ID, 25);
//		SubjectInfo.GetComponent<InfoScript> ().Name = ID; 
		addressQuestion = "Location of the file from which readings will be taken";
		//GUI.Label (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (300+dialogPosCorrection)*Screen.height*sizeCorrection, 1000*Screen.width*sizeCorrection*scaleGUI, 1000*Screen.height*sizeCorrection*scaleGUI), addressQuestion, style); //for some reason this moves up instead of moving down when the y component is increased
		//address= GUI.TextField (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (270+space+dialogPosCorrection)*Screen.height*sizeCorrection, 300*sizeCorrection*Screen.width*scaleGUI, 30*sizeCorrection*Screen.height*scaleGUI),address, 1000);
		folder_question="Subject Directory Path";
		//GUI.Label (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (350+dialogPosCorrection)*Screen.height*sizeCorrection, 1000*Screen.width*sizeCorrection*scaleGUI, 1000*Screen.height*sizeCorrection*scaleGUI),folder_question, style);
		//SubjectInfo.GetComponent<InfoScript> ().folder_path=GUI.TextField (new Rect ((40+dialogPosCorrectionX)*Screen.width*sizeCorrection, (260+2*space+dialogPosCorrection)*Screen.height*sizeCorrection, 300*sizeCorrection*Screen.width*scaleGUI, 30*sizeCorrection*Screen.height*scaleGUI),SubjectInfo.GetComponent<InfoScript> ().folder_path, 70);
		SubjectInfo.GetComponent<InfoScript> ().voltAddress = address;
		board.GetComponent<GonControllerScript> ().address = address;
	}
	public void changeSensitivity(float sensitivity){
		MenuObject.GetComponent<MenuScript> ().changeSensitivity (sensitivity);
	}
	public void changeMaxAngle(float angle){ //added to a slider
		MenuObject = GameObject.Find("MenuObject"); 
		//this line has to be there otherwise Unity throws a NullReferenceException which is ACM because all of this 
		//is happening in the start function of Sensitivity.cs
		MenuObject.GetComponent<MenuScript> ().changeMaxAngle (angle);
	}

	void reverseBool(ref bool b){
	b = (b == true) ? false : true;
	}

	/// <summary>
	/// TEST CASES:
	/// 1) Note which axes are controlled by which sensor body, hit flip in the menu and they should control different 
	///    axes now
	/// 2) Complete one attempt and go to the next one, the settings should stay as they were
	/// </summary>
	public void flipGon(){
		if(flexMode)reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<ArdController>().flip);
		else reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().flip);
		reverseBool (ref MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().flip);
		MenuObject.GetComponent<MenuScript> ().board.GetComponent<GonControllerScript> ().setBoardRotation ();
		MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().resetPosition (); //ACM this works because transform just maps to whatever values it contains
	}
	public void changeMaze(){
		UnityEngine.Debug.Log (MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().totalMazes.ToString());
		if (activeMaze != MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().totalMazes) {
			MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().activeMaze = ++activeMaze;
		} else {
			activeMaze = 1;
			MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().activeMaze = activeMaze;
		}
		ChangeMazeButton.GetComponentInChildren<Text> ().text = "Assigned Maze: "+activeMaze.ToString();
	}
	public void change_direction_1(){
		reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<ArdController>().flip_sensor_1);
	}
	public void change_direction_2(){
		reverseBool (ref MenuObject.GetComponent<MenuScript>().board.GetComponent<ArdController>().flip_sensor_2);
	}
	static void splitString(string input,string[] stringArr){
		int startIndex = input.IndexOf('\t');
		int endIndex = input.LastIndexOf('\t');
		stringArr[0] = input.Substring(0,startIndex);
		stringArr[1] = input.Substring(endIndex+1,input.Length-endIndex-1);
		
		
	}


	/// <summary>
	/// EFFECTS: Calls the zeroSensors() function from ArdController 
	/// </summary>
	public void zeroSensors(){

		board.GetComponent<ArdController> ().zeroSensors ();


	}

	public void changeGameSettings(){
		SensField.gameObject.SetActive (true);
		SensitivityObject.GetComponent<Sensitivity> ().changeSensitivity ();
		MaxAngleObject.GetComponent<MaxAngle> ().changeMaxAngle ();
		//since changeMaxAngle is called after changeSensitivity, the text on the game settings button is "Done"
		//the pullup operations are all handled by Sensitivity.cs
	}


	//EFFECTS: Calculates resitances from the menu using the menu's own serial port.  Resitances must be measured 
	//at all times for calibration(zeroing the sensors).  So this must be called from update
	//The resitances are still stored in the R1,R2,R3 & R4 variables in ArdController
	public void getResistanceFromMenu(){
		try{
		string ardString = ardPort.ReadLine ();
		string initString = "";
		//UnityEngine.Debug.Log(ardString);
		string[] voltReadings = new String[4];
		board.GetComponent<ArdController>().splitStringInto(ardString,voltReadings,4); //voltReadings now contains the different voltage readings
		//printArray(voltReadings,4);
		double V1,V2,V3,V4;
		V1 = Convert.ToDouble(voltReadings[0])*5/1023;
		V2 = Convert.ToDouble(voltReadings[1])*5/1023;
		V3 = Convert.ToDouble(voltReadings[2])*5/1023;
		V4 = Convert.ToDouble(voltReadings[3])*5/1023;
		


		board.GetComponent<ArdController>().computeResitances(V1,V2,V3,V4);
		}
		catch(SystemException){
		}

		UnityEngine.Debug.Log (board.GetComponent<ArdController>().R1.ToString()+" "+board.GetComponent<ArdController>().R2.ToString()+" "+board.GetComponent<ArdController>().R3.ToString()+" "+board.GetComponent<ArdController>().R4.ToString()+" ");

	}

	//Creates a directory if it does not exist and corresponding super folders
	public void createFolder(string folder_path){
		string newFolders = "";
		while (!Directory.Exists(folder_path)) {
			newFolders = Path.GetFileName (folder_path) + "/" + newFolders;
			folder_path = Path.GetDirectoryName (folder_path);
		}
		Directory.CreateDirectory (folder_path + "/" + newFolders);
	}

	public void flipSensorMode(){
		flexMode=(flexMode)?false:true;
		if (flexMode) {
			GameCalibrationButton.SetActive (true);
			//SensorCalibrationButton.SetActive (true);
			//zeroFlexButton.SetActive (true);
			//zeroGonButton.SetActive (false);
			CalibrationButton.SetActive (false);
			AddressQuestionText.SetActive (false);
			fileAddressField.gameObject.SetActive (false);
		} else {
			GameCalibrationButton.SetActive (false);
			SensorCalibrationButton.SetActive (false);
			//zeroFlexButton.SetActive (false);
			//zeroGonButton.SetActive (true);
			CalibrationButton.SetActive (true);
			AddressQuestionText.SetActive (true);
			fileAddressField.gameObject.SetActive (true);
		}
	}

	public void initNameField(){
		nameField.text = SubjectInfo.GetComponent<InfoScript> ().Name;
	}
		

	public void setSubjectName(string Name){
		SubjectInfo.GetComponent<InfoScript> ().Name = Name;
	}

	public void initFileAddressField(){
		fileAddressField.text = SubjectInfo.GetComponent<InfoScript> ().voltAddress;
	}

	public void setGonFileAddress(string address){
		SubjectInfo.GetComponent<InfoScript> ().voltAddress = address;
		board.GetComponent<GonControllerScript> ().address = address;
	}

	public void initAddressField(){
		addressField.text = SubjectInfo.GetComponent<InfoScript> ().folder_path;
	}

	public void setFolderPath(string path){
		SubjectInfo.GetComponent<InfoScript> ().folder_path = path;
	}

	public void initSerialPortField(){
		SerialPortField.text = board.GetComponent<ArdController> ().portString;
	}

	public void onSerialPortEntered(string serialPort){
		board.GetComponent<ArdController> ().portString = serialPort;
		try{
			ardPort = new SerialPort(board.GetComponent<ArdController>().portString);
			ardPort.Open ();
		}
		catch(IOException e){
			//ardPortSuccess = false;
		}
	}

	public string AutodetectArduinoPort(){
		string[] ports = SerialPort.GetPortNames ();

		UnityEngine.Debug.Log ("Number of ports: " + ports.Length);
		foreach (string port in ports) {
			UnityEngine.Debug.Log (port);
		}

		return "";
	}

	public void switchJoint1(){
		board.GetComponent<ArdController> ().switchOffJoint1 = !board.GetComponent<ArdController> ().switchOffJoint1;
	}
	public void switchJoint2(){
		board.GetComponent<ArdController> ().switchOffJoint2 = !board.GetComponent<ArdController> ().switchOffJoint2;
	}

}
