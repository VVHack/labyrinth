﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CalibrationScript : MonoBehaviour {
	
	public bool calibrationScene = false;
	public GameObject Player;
	public GameObject board;
	public GameObject Deactivated;
	public GameObject SubjectInfo;
	GameObject[] objectsToSave;

	public GameObject MenuObject;
	public GameObject backCal;


	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);//must be destroyed when going back from the parent menu scene
		//the parent menu scene is the scene where this object initially exists
		//there is no parent menu scene for the MenuObject
		calibrationScene=false;
		LoadObjects ();

		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	public void LoadObjects(){
		objectsToSave = UnityEngine.Object.FindObjectsOfType<GameObject> ();

	}
	public void LoadCalScene(){
		

		if (!calibrationScene) {
			calibrationScene=true;
			foreach (object go in objectsToSave) {
				GameObject g = (GameObject)go;
				if(g.name=="Board") board=g;
				if(g.name=="Player") Player=g;
				if(g.name=="Deactivated") Deactivated=g;
				if(g.name=="SubjectInfo") SubjectInfo=g;

				if(g.name=="MenuObject") MenuObject=g;


				
				if(g.transform.parent==null ) {
					g.transform.SetParent(transform);
					
					if(g.name!=name && g.name!=MenuObject.name )
						g.SetActive (false);
					//The MenuObject is not deactivated so if the user unexpectedly exits in the calibration scene, the file deletion will be taken care of
					
				}
			}

			Application.LoadLevel(5);
			backCal=GameObject.Find ("Back");
			Deactivated.SetActive(true);
			
			
			
		}
		else if (calibrationScene) {

			
			calibrationScene=false;
			foreach (object go in objectsToSave) {
				GameObject g = (GameObject)go;
				if(g.transform.parent==transform ){
					g.SetActive (true);
					g.transform.SetParent(null);
				}
			}
		}
		
	}
}

