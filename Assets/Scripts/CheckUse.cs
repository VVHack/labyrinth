﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckUse : MonoBehaviour {
	GameObject MenuObject;
	public Text t;
	// Use this for initialization
	void Start () {
		MenuObject = GameObject.Find ("MenuObject");
	
	}
	void checkUse(){
		if (MenuObject.GetComponent<MenuScript>().board.GetComponent<BoardController>().keyBoardUse) {
			//UnityEngine.Debug.Log ("It is on");
			t.text = "KeyBoard Mode: On";
			MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().NewAng1.text="";
			MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().NewAng2.text="";
			MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().TextAngD1.text="";
			MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().TextAngD2.text="";
			
		}
		else  
			t.text = "KeyBoard Mode: Off";
	}
	// Update is called once per frame
	void Update () {
		checkUse ();
	
	}
}
