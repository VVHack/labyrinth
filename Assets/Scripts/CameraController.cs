﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	//private GameObject player;
	//Vector3 offset;
	GameObject board;
	bool keepBoardAligned=true;
	// Use this for initialization
	void Start () {
		//player = GameObject.Find ("Player");
		//offset = player.transform.rotation - transform.rotation;
		board = GameObject.Find ("Board");

	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 rot = board.transform.rotation.eulerAngles;
		if(keepBoardAligned==true)transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, rot.y, 0);
	
	
	}
}
