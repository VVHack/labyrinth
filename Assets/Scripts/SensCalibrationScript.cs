﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using System.IO.Ports;


//Use this to determine resistance-angle relationship when calibrating on the spot

//WARNING: THIS IS NOT ATTACHED TO SensorCalibrationObject, SensorCalibrationButtonScript IS
//This script is the equivalent of GoBack.cs



public class SensCalibrationScript : MonoBehaviour {

	public bool calibratingSensor1 = true; //whether the front or rear sensor is being calibrated
	public bool measuringJointAngle = true;
	public bool measuringInitial = true; //whther we are measuring the value for the initial angle or the final angle
	public Text buttonText_1; //the text object in the sensor toggle button
	public Text jointDescriptionText; //the text displaying information as to which joint is being calibrated
	                                  //the text component of the joint toggle button
	public int sensCalibrationPhase = 1; //the joint for which we are calibrating right now
	public Text angleText; //the text object containging information about the kind of angle we are calibrating with
	public bool joint1 = true; //the boolean indicating the joint for which the sensors are being calibrated
	string initialAngle = "0",finalAngle = "0";
	double m,c;
	public GameObject MenuObject,board,SensorCalibrationObject, SubjectInfo;
	public SerialPort ardPort;
	public double Rinit, Rfinal;
	float sizeCorrection = 0.001f;
	public GUIStyle style;




	// Use this for initialization
	void Start () {
		SensorCalibrationObject = GameObject.Find ("SensorCalibrationObject");
		MenuObject = SensorCalibrationObject.GetComponent<SensCalibrationButtonScript> ().MenuObject;
		board = MenuObject.GetComponent<MenuScript> ().board;
		SubjectInfo = MenuObject.GetComponent<MenuScript> ().SubjectInfo;
		try{
			ardPort = new SerialPort (board.GetComponent<ArdController>().portString);
			ardPort.Open();
		}
		catch(IOException e){}


		//board = GameObject.Find ("SensorCalibrationObject/MenuObject/Board");
	
	}
	
	// Update is called once per frame
	void Update () {
		getResistanceFromMenu ();
	
	}

	//EFFECTS: Called when the button ResistorToggle is called. Toggles between the forward and rear sensor for calibration
	public void resistorToggle(){
		calibratingSensor1 = (calibratingSensor1)?false:true;
		if (calibratingSensor1) {
			buttonText_1.text = "Forward Sensor";
		} else {
			buttonText_1.text = "Rear Sensor";
		}
	}
	public void angleToggle(){
		measuringInitial = (measuringInitial)?false:true;
		if (measuringInitial) {
			angleText.text = "Measuring for intial angle";
		} else {
			angleText.text = "Measuring for final angle";
		}
	}
	public void jointToggle(){
		joint1 = (joint1)?false:true;
		if (joint1) {
			jointDescriptionText.text = "Joint 1";
		} else {
			jointDescriptionText.text = "Joint 2";
		}
	}

	public void goBack(){
		GameObject camera = GameObject.Find ("Main Camera");
		GameObject light = GameObject.Find ("Directional Light");
		GameObject EventSystem = GameObject.Find ("EventSystem");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject Background = GameObject.Find ("Background");
		//Destroy (camera);
		Destroy (Background);
		Destroy (camera);
		Destroy (light);
		Destroy (EventSystem);
		Destroy (Canvas); //All buttons and texts would be destroyed with this 

		SensorCalibrationObject.GetComponent<SensCalibrationButtonScript> ().LoadMenu ();

		Destroy (gameObject);
	}


	void OnGUI(){
		GUI.skin.textField.fontSize = 10 * (int)Math.Ceiling(Screen.width * sizeCorrection);
		//GUI.contentColor = Color.red;
		//GUI.backgroundColor = Color.yellow;
		//GUI.Label (new Rect (30*Screen.width*sizeCorrection, 270*Screen.height*sizeCorrection, 1000*Screen.width*sizeCorrection, 1000*Screen.height*sizeCorrection), "Initial angle for measurement","style");
		initialAngle = GUI.TextField (new Rect (240*Screen.width*sizeCorrection, 290*Screen.height*sizeCorrection, 50*Screen.width*sizeCorrection, 30*Screen.height*sizeCorrection), initialAngle, 10000);
		if (!initialAngle.Contains ("."))
			initialAngle = initialAngle + ".0";
		//GUI.Label (new Rect (270*Screen.width*sizeCorrection, 270*Screen.height*sizeCorrection, 1000*Screen.width*sizeCorrection, 1000*Screen.height*sizeCorrection), "Final angle for measurement");
		finalAngle = GUI.TextField (new Rect (240*Screen.width*sizeCorrection, 350*Screen.height*sizeCorrection, 50*Screen.width*sizeCorrection, 30*Screen.height*sizeCorrection), finalAngle, 10000);
		if (!finalAngle.Contains ("."))
			finalAngle = finalAngle + ".0";


		/*TEST CASES
		 1) Start with everything 0, switch the toggles and obseve the values for the gradiesnts
		  and intercets for all the joints
		 2)Input: initialAngle = 10, finalAngle = 50, measuringJointAngle
		   m should be 0.444444... and c should be 10
		 3)Input: initialAngle = 30, finalAngle = 60, !measuringJointAngle
		   m should be -0.333333.... and c should be 150

		*/

		/*TEST CASES
		1) With front sensor mode turned on and joint set to joint 1, perform calibration
		   on sensor 1, observe values, they should match with the values for mForward1 and cForward1
		2) Switch to rear sensor and calibrate sensor 2 and observe values for mRear1 and cRear1
	    3) Swtich back to forward sensor and change to joint 2.  Calibrate sensor 3, observe values,they should match with
		   the values for mForward2 and mRear2
		4) Switch to rear sensor and calibrate sensor 4, observe values, they should match with the values for mRear2 
           and mRear2.
		
		Enter appropriate angles for all of them
        Look at whether the values for Rinit and Rfinal change with the correct sensor
		*/

		try{
			double R1 = board.GetComponent<ArdController>().R1;
			double R2 = board.GetComponent<ArdController>().R2;
			double R3 = board.GetComponent<ArdController>().R3;
			double R4 = board.GetComponent<ArdController>().R4;
			if(measuringInitial)Rinit = (joint1)?((calibratingSensor1)?R1:R2):((calibratingSensor1)?R3:R4);
			else Rfinal = (joint1)?((calibratingSensor1)?R1:R2):((calibratingSensor1)?R3:R4);
			double deltaR = Rfinal - Rinit;
			if(joint1){
				if(calibratingSensor1){
										board.GetComponent<ArdController>().mForward1 = deltaR / (Convert.ToDouble(finalAngle) - Convert.ToDouble(initialAngle));
					board.GetComponent<ArdController>().cForward1 = Rinit - (Convert.ToDouble(initialAngle)*board.GetComponent<ArdController>().mForward1);
										SubjectInfo.GetComponent<InfoScript>().mForward1 = board.GetComponent<ArdController>().mForward1;
										SubjectInfo.GetComponent<InfoScript>().cForward1 = board.GetComponent<ArdController>().cForward1;
				} else {
										board.GetComponent<ArdController>().mRear1 = deltaR / (Convert.ToDouble(finalAngle) - Convert.ToDouble(initialAngle));
					board.GetComponent<ArdController>().cRear1 = Rinit - (Convert.ToDouble(initialAngle)*board.GetComponent<ArdController>().mRear1);
										SubjectInfo.GetComponent<InfoScript>().mRear1 = board.GetComponent<ArdController>().mRear1;
										SubjectInfo.GetComponent<InfoScript>().cRear1 = board.GetComponent<ArdController>().cRear1;

				}

			} else {
				if(calibratingSensor1){
					board.GetComponent<ArdController>().mForward2 = deltaR / (Convert.ToDouble(finalAngle) - Convert.ToDouble(initialAngle));
					board.GetComponent<ArdController>().cForward2 = Rinit - (Convert.ToDouble(initialAngle)*board.GetComponent<ArdController>().mForward2);
										SubjectInfo.GetComponent<InfoScript>().mForward2 = board.GetComponent<ArdController>().mForward2;
										SubjectInfo.GetComponent<InfoScript>().cForward2 = board.GetComponent<ArdController>().cForward2;
				} else {
					board.GetComponent<ArdController>().mRear2 = deltaR / (Convert.ToDouble(finalAngle) - Convert.ToDouble(initialAngle));
					board.GetComponent<ArdController>().cRear2 = Rinit - (Convert.ToDouble(initialAngle)*board.GetComponent<ArdController>().mRear2);
										SubjectInfo.GetComponent<InfoScript>().mRear2 = board.GetComponent<ArdController>().mRear2;
										SubjectInfo.GetComponent<InfoScript>().cRear2 = board.GetComponent<ArdController>().cRear2;
				}
			}
		}
		catch(FormatException e){ //in case the user types a character that is not a number or a decimal point
		}



//		try{
//		c = Convert.ToDouble (initialAngle);
//		m = (Convert.ToDouble (finalAngle) - Convert.ToDouble (initialAngle)) / 90;
//		if (!measuringJointAngle) {
//			c = 180 - c;
//			m = -m;
//		}
//		if (joint1) {
//			if (calibratingSensor1) {
//				board.GetComponent<ArdController> ().mForward1 = m;
//				board.GetComponent<ArdController> ().cForward1 = c;
//			} else {
//				board.GetComponent<ArdController> ().mRear1 = m;
//				board.GetComponent<ArdController> ().cRear1 = c;
//			}
//		} else {
//			if (calibratingSensor1) {
//				board.GetComponent<ArdController> ().mForward2 = m;
//				board.GetComponent<ArdController> ().cForward2 = c;
//			} else {
//				board.GetComponent<ArdController> ().mRear2 = m;
//				board.GetComponent<ArdController> ().cRear2 = c;
//			}
//		}
//	}
//		catch(FormatException e){ //in case the user types a character that is not a number or a decimal point
//		}
	}

	//EFFECTS: Calculates resitances from the menu using the menu's own serial port.  Resitances must be measured 
	//at all times for game calibration so this function must be called in update.
	//The resitances are still stored in the R1,R2,R3 & R4 variables in ArdController
	public void getResistanceFromMenu(){
		try{
			string ardString = ardPort.ReadLine ();
			string initString = "";
			//UnityEngine.Debug.Log(ardString);
			string[] voltReadings = new String[4];
			board.GetComponent<ArdController>().splitStringInto(ardString,voltReadings,4); //voltReadings now contains the different voltage readings
			//printArray(voltReadings,4);
			double V1,V2,V3,V4;
			V1 = Convert.ToDouble(voltReadings[0])*5/1023;
			V2 = Convert.ToDouble(voltReadings[1])*5/1023;
			V3 = Convert.ToDouble(voltReadings[2])*5/1023;
			V4 = Convert.ToDouble(voltReadings[3])*5/1023;



			board.GetComponent<ArdController>().computeResitances(V1,V2,V3,V4);
		}
		catch(SystemException){
		}

		UnityEngine.Debug.Log (board.GetComponent<ArdController>().R1.ToString()+" "+board.GetComponent<ArdController>().R2.ToString()+" "+board.GetComponent<ArdController>().R3.ToString()+" "+board.GetComponent<ArdController>().R4.ToString()+" ");

	}

}
