﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class InfoScript : MonoBehaviour {
	//Initial values for game variables to be copied by BoardController and BallController in their Start() function
	public string Name="Foo";  //stores the name of the subject 
	public int attempts=0;
	public string voltAddress;
	public Stream loadDataStream;
	public StreamWriter loadData;
	public int fileIndex;
	public float sensitivity=10f;
	public float maxSensitivity=150f;
	public float maxAngle=20f;
	public bool flip=false;
	public double scale1=36;
	public double scale2=36;
	public double tempScale1=0,tempScale2=0;
	public double kneeZero=0,kneeMax;
	public double hipMin,hipMax,hipZero=0;
	public GameObject Files;
	string csv_separator = ", ";
	public GameObject Player;
	public int gameObjectCount=0;
	public string Dof1="",Dof2="";
	bool justLoaded=false;
	public double anat_zero_1,anat_zero_2;
	public int sign_1=1,sign_2=1;
	public double Dof1_Max=-180,Dof1_Min,Dof1_Neutral,Dof1_Calibration_Max, Dof1_Calibration_Min,Dof2_Max, Dof2_Min, Dof2_Neutral, Dof2_Calibration_Max, Dof2_Calibration_Min, Dof1_Input_Max, Dof1_Input_Min, Dof1_Input_Neutral, Dof1_Input_Calibration_Max, Dof1_Input_Calibration_Min, Dof2_Input_Max, Dof2_Input_Min, Dof2_Input_Neutral, Dof2_Input_Calibration_Max, Dof2_Input_Calibration_Min;
	public string folder_path="/Users/vedantgupta/Desktop/Roll a Ball";
	public bool flip_direction_1=false,flip_direction_2=false;
	public StreamWriter loadRawData;
	public int mode_count=0;
	public int chosen_value_1,chosen_value_2;
	public StreamWriter loadTrajectory;
	public int menuGameCount=0;
	public Text questionText; //assigned from the scene
	public GameObject yesButton, noButton; //assigned from the scene 
	public GameObject continueButton, customizeButton; //assigned from the scene 
	public bool playingAsFoo = false;
	public bool usingDefaultSettings = false;
	public int toggle_count = 0;
	public double m=0.0782,c=8.9221;
	float t=0,welcomeTime=1;
	bool gameStarted = false;
	public double mForward1, cForward1, mRear1, cRear1;
	public double mForward2, cForward2, mRear2, cRear2;
	public double highScore=Double.PositiveInfinity, lowScore=0;
	public List<float> scores;
	public bool keyboardUse = true;

	public double maxResistance_1, minResistance_1, maxResistance_2, minResistance_2;


	// Use this for initialization
	void Start () {;
		DontDestroyOnLoad (gameObject);

		//Below is the process for creating the folder if it doesn't already exist to hold the subject data
		folder_path = Directory.GetCurrentDirectory ();
		UnityEngine.Debug.Log (Path.GetFileName (folder_path));
		while (Path.GetFileName (folder_path) != System.Environment.UserName) {
			folder_path = Path.GetDirectoryName (folder_path);
		}
		folder_path += "/Documents/NeuRRoLabyrinth";
		if (!Directory.Exists (folder_path)) {
			//Directory.CreateDirectory (folder_path);
		}
		folder_path += "/Subjects";
		if (!Directory.Exists (folder_path)) {
			//Directory.CreateDirectory (folder_path);
		}
		Name = "Foo";
		//SceneManager.LoadScene (1);
	
	}
	
	// Update is called once per frame
	void Update () {
		t += Time.deltaTime;
		if (t > welcomeTime && !gameStarted) {
			gameStarted = true;
			SceneManager.LoadScene (1);
		}
	
	}


	//GONIOMETER IMPLEMENTATION
//	//Loads data to the attempt data file after a successful attempt
//	public void loadAttemptData(float time){
//
//
//		try{
//		loadData.Write (Name+", "+DateTime.Now.ToString("HH:mm:ss tt")+", "+attempts.ToString()+", "+time.ToString()+", "+Dof1+", "+Dof1_Max.ToString()+", "+Dof1_Min.ToString()+", "+(36*sign_1*(kneeZero-anat_zero_1)).ToString ()+", "+Dof1_Calibration_Max.ToString()+", "+Dof1_Calibration_Min.ToString()+", "+Dof2+", "+Dof2_Max.ToString()+", "+Dof2_Min.ToString()+", "+ (36*sign_2*(hipZero-anat_zero_2)).ToString ()+", "+Dof2_Calibration_Max.ToString()+", "+Dof2_Calibration_Min.ToString()+", "+((Dof1_Max/(36*sign_1))+anat_zero_1).ToString()+", "+((Dof1_Min/(36*sign_1))+anat_zero_1).ToString()+", "+kneeZero.ToString ()+", "+((Dof1_Calibration_Max/(36*sign_1))+anat_zero_1).ToString()+", "+((Dof1_Calibration_Min/(36*sign_1))+anat_zero_1).ToString()+", "+((Dof2_Max/(36*sign_2))+anat_zero_2).ToString()+", "+((Dof2_Min/(36*sign_2))+anat_zero_2).ToString()+", "+hipZero.ToString()+", "+((Dof2_Calibration_Max/(36*sign_2))+anat_zero_2).ToString().ToString()+", "+((Dof2_Calibration_Min/(36*sign_2))+anat_zero_2).ToString()+"\n");
//		}
//		catch(NullReferenceException e){}
//
//	}

	//Loads data to the attempt data file after a successful attempt
	public void loadAttemptData(float time){
		

		try{
			loadData.Write (Name+", "+DateTime.Now.ToString("HH:mm:ss tt")+", "+attempts.ToString()+", "+time.ToString()+", "+Dof1+", "+Dof1_Max.ToString()+", "+Dof1_Min.ToString()+", "+((1/m)*(kneeZero-anat_zero_1)*sign_1).ToString ()+", "+Dof1_Calibration_Max.ToString()+", "+Dof1_Calibration_Min.ToString()+", "+Dof2+", "+Dof2_Max.ToString()+", "+Dof2_Min.ToString()+", "+ ((1/m)*(hipZero-anat_zero_2)*sign_2).ToString ()+", "+Dof2_Calibration_Max.ToString()+", "+Dof2_Calibration_Min.ToString()+", "+(anat_zero_1 + m*Dof1_Max*sign_1).ToString()+", "+(Dof1_Min*m*sign_1 +anat_zero_1).ToString()+", "+kneeZero.ToString ()+", "+(anat_zero_1 + m*Dof1_Calibration_Max*sign_1).ToString()+", "+(anat_zero_1 + m*Dof1_Calibration_Min*sign_1).ToString()+", "+(anat_zero_2 + Dof2_Max*m*sign_2).ToString()+", "+(anat_zero_2 + Dof2_Min*m*sign_2).ToString()+", "+hipZero.ToString()+", "+(anat_zero_2 + Dof2_Calibration_Max * m * sign_2).ToString().ToString()+", "+(anat_zero_2 + m * Dof2_Calibration_Min * sign_2).ToString()+"\n");
		}
		catch(NullReferenceException e){}

	}

	//Writes headings to the subject data file
	public void loadSubjectData(){
		loadData.Write ("Subject_ID, Timestamp, Trial_number, Trial_time, Dof1, Dof1_Max, Dof1_Min, Dof1_Neutral, Dof1_Calibration_Max, Dof1_Calibration_Min, Dof2, Dof2_Max, Dof2_Min, Dof2_Neutral, Dof2_Calibration_Max, Dof2_Calibration_Min, Dof1_Input_Max, Dof1_Input_Min, Dof1_Input_Neutral, Dof1_Input_Calibration_Max, Dof1_Input_Calibration_Min, Dof2_Input_Max, Dof2_Input_Min, Dof2_Input_Neutral, Dof2_Input_Calibration_Max, Dof2_Input_Calibration_Min\n");
	}


	//EFFECTS: Called when the noButton is pressed. Sets usingDefaultSettiings to true.  This causes BoardController to not load the menu scene.
	//         Creates the directory. The necessary file streams are created when the main game scene is loaded.
	//         Loads scene 1, the main game scene.
	public void changeNoSettings(){
		menuGameCount = 1;
		usingDefaultSettings = true;
		create_subject_directory ();
		SceneManager.LoadScene (1);
	}

	//EFFECTS: Called when the yes button is pressed.  This sets usingDefaultSettings to false and loads level 1, the main game scene
	//         where BoardController redirects to the menu. 
	//         The net effect is that the name of the person is simply set to Foo
	public void changeSettings(){
		usingDefaultSettings = false;
		SceneManager.LoadScene (1);
	}

	//EFFECTS: Called by contnueAsFoo().  Changes questionText to ask the question about using default settings and activates the yes and no buttons. 
	//         Shifts the text as well to make it align with the center
	void askQuestionForDefaultSettngs(){
		questionText.text = "Do you want to change any settings?";
		questionText.transform.Translate (new Vector3(-11,0,0));
		yesButton.SetActive (true);
		noButton.SetActive (true);
		continueButton.SetActive (false);
		customizeButton.SetActive (false);
	}

	//EFFECTS: Called when the continueButton is pressed.  Sets playingAsFoo to true 
	//         Sets the name as Foo and the folder_path as the path the current directory and then calls the askQuestionForDefaultSettings() function
	public void continueAsFoo(){
		playingAsFoo = true;
		Name = "Foo";
		askQuestionForDefaultSettngs ();
	}

	//EFFECTS: Called when the customizeButton is pressed.  Sets playingAsFoo to false and loads the game normally with the BoardController
	//         redirecting to a blank menu scene
	public void customizeInfo(){
		playingAsFoo = false;
		SceneManager.LoadScene(1);
	}

	public void create_subject_directory(){
		folder_path+="/"+Name+"_";
		int i = 0;
		while (Directory.Exists(folder_path+(++i).ToString())) {

		}
		folder_path += i.ToString ();
		UnityEngine.Debug.Log ("The created directory is "+ folder_path);
		Directory.CreateDirectory (folder_path+="/");
	}
	void OnApplicationQuit(){
		File.Delete ("record.txt"); //so that this file used only for runtime purposes does not crowd the host computer
		                            //this is not delted from BallController because it won't be deleted if the game 
		                            //is exited in between attempts as the Player would be deactivated
	}
}
