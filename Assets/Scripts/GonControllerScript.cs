﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System;
using UnityEngine.UI;
using System.Text;
using System.Runtime.InteropServices;

public class GonControllerScript : MonoBehaviour {
    double angularDisplacement=0;
    public double currentAngle=0;
    public double newAngle=0;
    double angularDisplacement2 = 0;
    public double currentAngle2 = 0;
    public double newAngle2 = 0;
	public double kneeScale,hipScale;
	public double kneeZero=0,kneeMax;
	public double hipMin,hipMax,hipZero=0;
    Stream stream;
    int frequency;
    public Text text;
    public Text text2;
    StringReader readLine;
	GameObject SubjectInfo;
    StreamReader streamReader;
	public string address;
	public bool keyboard=true;
	public Text TextAngD1,TextAngD2,NewAng1,NewAng2;
	public bool flip=false;
	public double scale1=36;
	public double scale2=36;
	public double tempScale1=0,tempScale2=0;
	public GameObject Player;
	public double Dof1,Dof2;
	public double Dof1_anat=0, Dof2_anat=0;
	public double anat_zero_1,anat_zero_2;
	public int sign_1=1, sign_2=1;
	public double Dof1_Max=-180,Dof1_Min=180,Dof1_Neutral,Dof1_Calibration_Max, Dof1_Calibration_Min,Dof2_Max=-180, Dof2_Min=180, Dof2_Neutral, Dof2_Calibration_Max, Dof2_Calibration_Min, Dof1_Input_Max, Dof1_Input_Min, Dof1_Input_Neutral, Dof1_Input_Calibration_Max, Dof1_Input_Calibration_Min, Dof2_Input_Max, Dof2_Input_Min, Dof2_Input_Neutral, Dof2_Input_Calibration_Max, Dof2_Input_Calibration_Min;
	public bool flip_direction_1=false,flip_direction_2=false;

	public void showInfo(){
		TextAngD1.text = "Angular Displacement 1= " + angularDisplacement.ToString ();
		TextAngD2.text = "Angular Displacement 2= " + angularDisplacement2.ToString ();
		NewAng1.text = "Dof1= " + Dof1.ToString ();
		NewAng2.text = "Dof2= " + Dof2.ToString ();

	}
    
	// Use this for initialization
	void Start () {
       
		angularDisplacement=0;
		currentAngle=0;
		newAngle=0;
		angularDisplacement2 = 0;
		currentAngle2 = 0;
		newAngle2 = 0;
		TextAngD1.text = "";
		TextAngD2.text = "";
		NewAng1.text = "";
		NewAng2.text = "";

		SubjectInfo = GameObject.Find ("SubjectInfo");

		//get necessary information for SubjectInfo
		flip=SubjectInfo.GetComponent<InfoScript>().flip;
		scale1=SubjectInfo.GetComponent<InfoScript>().scale1;
		scale2=SubjectInfo.GetComponent<InfoScript>().scale2;
		tempScale1 = SubjectInfo.GetComponent<InfoScript> ().tempScale1;
		tempScale2 = SubjectInfo.GetComponent<InfoScript> ().tempScale2;
		kneeZero = SubjectInfo.GetComponent<InfoScript> ().kneeZero;
		hipZero = SubjectInfo.GetComponent<InfoScript> ().hipZero;
		anat_zero_1 = SubjectInfo.GetComponent<InfoScript> ().anat_zero_1;
		anat_zero_2 = SubjectInfo.GetComponent<InfoScript> ().anat_zero_2;
		sign_1 = SubjectInfo.GetComponent<InfoScript> ().sign_1;
		sign_2 = SubjectInfo.GetComponent<InfoScript> ().sign_2;

		
		//the address from where the details shall be read
		address = SubjectInfo.GetComponent<InfoScript> ().voltAddress;
	
	}

	public void setBoardRotation(){
		transform.Rotate (-transform.rotation.eulerAngles.x,-transform.rotation.eulerAngles.y,-transform.rotation.eulerAngles.z,Space.World);
	}

	// Update is called once per frame
	void Update () {

		try{
		stream = File.Open (@address, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //@"C:\Users\PM&R\Desktop\data.txt"
		
		
		streamReader = new StreamReader (stream);
		StringBuilder sb = new StringBuilder ();
		StringWriter wrt = new StringWriter (sb);
		StringReader rd;
		if (streamReader.Peek () != -1) {
			
			string inputString = streamReader.ReadLine ();
			wrt.WriteLine (inputString);
			String [] numStrings = new String[2];
			String string1, string2;
			splitString (inputString, numStrings);

			Dof1=36*(Convert.ToDouble (numStrings [0])-anat_zero_1)*sign_1;
			
			Dof1_Max= (Dof1>Dof1_Max)?Dof1:Dof1_Max;
			
			SubjectInfo.GetComponent<InfoScript>().Dof1_Max=Dof1_Max;
			
			Dof1_Min= (Dof1<Dof1_Min)?Dof1:Dof1_Min;
			
			SubjectInfo.GetComponent<InfoScript>().Dof1_Min=Dof1_Min;
			
			Dof2=36*(Convert.ToDouble (numStrings [1])-anat_zero_2)*sign_2;
			
			Dof2_Max= (Dof2>Dof2_Max)?Dof2:Dof2_Max;
			
			SubjectInfo.GetComponent<InfoScript>().Dof2_Max=Dof2_Max;
			
			Dof2_Min= (Dof2<Dof2_Min)?Dof2:Dof2_Min;
			
			SubjectInfo.GetComponent<InfoScript>().Dof2_Min=Dof2_Min;
			
			newAngle = Convert.ToDouble (numStrings [0])-kneeZero;
			newAngle2 = Convert.ToDouble (numStrings [1])-hipZero;
				try{
			SubjectInfo.GetComponent<InfoScript>().loadRawData.WriteLine(Player.GetComponent<BallControler>().t.ToString()+", "+Dof1.ToString()+", "+Dof2.ToString()+", "+numStrings[0]+", "+numStrings[1]);
				}
				catch(ArgumentException e){}
				catch(NullReferenceException e){}


		if (!keyboard) {

			angularDisplacement = newAngle - currentAngle;
			angularDisplacement2 = newAngle2 - currentAngle2;
			if(angularDisplacement>=0.1 || angularDisplacement2>=0.1){
				Player.transform.parent=transform;
			}
			else{
				Player.transform.parent=null;
			}
			showInfo ();
			if(flip){
				double temp=angularDisplacement;
				angularDisplacement=angularDisplacement2; //this way angularDisplacement always refers to the knee and angularDisplacement2 always refers to the hip
				angularDisplacement2=temp;
			}
			if(flip_direction_1){
				angularDisplacement*=-1;
			}
			if(flip_direction_2){
				angularDisplacement2*=-1;
			}
			scale1=tempScale1*GetComponent<BoardController>().maxAngle;
			scale2=tempScale2*GetComponent<BoardController>().maxAngle;
			angularDisplacement*=scale1;
			angularDisplacement2*=scale2;
			Player.transform.parent=transform;
			//Player.transform.parent = transform;
			transform.Rotate ((float)angularDisplacement, 0, (float)angularDisplacement2, Space.World);
			if (Player != null)
				Player.GetComponent<BallControler> ().moveBall ();
					//Player.transform.parent = null;
			currentAngle = newAngle;
			currentAngle2 = newAngle2;

			

				}}
		}//End try bock
		catch(ArgumentException e){
		}
		catch(FileNotFoundException e){}


	


        

	
	}
	    static void splitString(string input,string[] stringArr){
	        int startIndex = input.IndexOf('\t');
	       int endIndex = input.LastIndexOf('\t');
	       stringArr[0] = input.Substring(0,startIndex);
	       stringArr[1] = input.Substring(endIndex+1,input.Length-endIndex-1);
	       
	        
	    }


}













