﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
 

public class Sensitivity : MonoBehaviour {
	public Slider slider; //to be set from the game scene
	GameObject MenuObject;
	string initName; //currently extracted from the text object of the button
	//public InputField SensInput;
	public Text SensText; //to be set from the game scene
	public bool type_in=false;
	bool changing=false;
	string value;
	int xCorrection = 150,yCorrection = 200;
	float sizeCorrection = 0.001f;
	bool valueIsNumeric = true;
	public InputField SensInputField;
	public Text sensInputText, sensPlaceholder, sensWarningText;
	// Use this for initialization
	void Start () {
		slider.gameObject.SetActive (false);
		//initName = gameObject.GetComponentInChildren<Text>().text;
		MenuObject = GameObject.Find ("MenuObject"); 
		slider.value = MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity;
		value= (MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity).ToString();
		SensText.text = (100 * slider.value).ToString () + " %";
	
	}
	public void changeSensitivity(){
		//sensitivity will be reset to the original in the next game
		if (transform.parent.gameObject.GetComponentInChildren<Text>().text != "Done") {
			MenuObject.GetComponent<MenuScript> ().pullUp (transform.parent.gameObject);
			SensText.gameObject.SetActive(true);
			slider.gameObject.SetActive (true);
			changing=true;
			slider.value = MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity;

			initSensField ();
			value = (100*slider.value).ToString();
			//SensInput.gameObject.SetActive(true);
		} else {
			SensInputField.gameObject.SetActive (false);
			slider.gameObject.SetActive (false);
			SensText.gameObject.SetActive(false);
			transform.parent.gameObject.GetComponentInChildren<Text>().text="Game Settings";
			MenuObject.GetComponent<MenuScript> ().pullUp (transform.parent.gameObject);
			changing=false;

		}
	}
	
	// Update is called once per frame
	void Update () {
		SensText.text = (100 * slider.value).ToString () + " %";

	
	}

	void initSensField(){
		SensInputField.text = (100*slider.value).ToString();
		//sensInputText.gameObject.SetActive (true);
		//sensPlaceholder.gameObject.SetActive (true);
	}

	void onSensEntered(string sens){
		sensWarningText.gameObject.SetActive (false);
		try{
			double s = Convert.ToDouble(sens);
			if(s>100*slider.maxValue){
				SensInputField.text = (100*slider.maxValue).ToString();
				slider.value = slider.maxValue;
			}
			else if(s<0){
				SensInputField.text = "0";
				slider.value = 0;
			}
			else{
				slider.value = (float)s/100;
			}
		}
		catch(FormatException e){
			sensWarningText.gameObject.SetActive (true);
		}
	}


	void OnGUI(){
		GUI.skin.textField.fontSize = 8 * (int)Math.Ceiling(Screen.width * sizeCorrection);
		if (changing == true) {
		try{
				if(value!="") value = (100*slider.value).ToString();
			//value = (100*MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity).ToString();
				//if(!value.Contains(".")) value=value+".0";
				value = (GUI.TextField (new Rect ((250+xCorrection)*Screen.width*sizeCorrection,(150+yCorrection)*Screen.height*sizeCorrection, 300*Screen.width*sizeCorrection, 30*Screen.height*sizeCorrection), value, 10000));
				double d;
				valueIsNumeric = double.TryParse(value, out d);
			}
			catch(FormatException e){
				value = (100*MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity).ToString();

			}
			catch(ArgumentOutOfRangeException e){
				value = (100*MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().sensitivity/MenuObject.GetComponent<MenuScript> ().Player.GetComponent<BallControler> ().maxSensitivity).ToString();
				
			}
			if (valueIsNumeric) {
				if (Convert.ToDouble (value) > 100*slider.maxValue) {
					value = (100 * slider.maxValue).ToString();
				}
				slider.value = ((float)Convert.ToDouble (value)) / 100;
			}

		}
	}
}
