﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.IO.Ports;
using UnityEngine.UI;

public class goBackGameCalibration : MonoBehaviour {
	public Text InstructionText;
	public Text nextText;
	GameObject MenuGameCalObject,board,SubjectInfo;
	public int phase = 0;
	public SerialPort ardPort;
	double Dof1_Calibration_Max, Dof2_Calibration_Max;
	public double maxResistance_1,minResistance_1,maxResistance_2,minResistance_2; //they are not exactly resistance
	                                                                       //they are the change in resistance
	public GameObject prevButton;
	public GameObject nextButton;

	public double jointAngle;

	public Text angleWarning;
	public Text startCalibration;

	public GameObject zeroButton;

	// Use this for initialization
	void Start () {
		MenuGameCalObject = GameObject.Find ("MenuGameCalObject");
		board = MenuGameCalObject.GetComponent<MenuGameCalScript> ().board;
		SubjectInfo = MenuGameCalObject.GetComponent<MenuGameCalScript> ().SubjectInfo;

		try{
		ardPort = new SerialPort (board.GetComponent<ArdController>().portString);
		ardPort.Open();
		}
		catch(IOException e){
		}
		maxResistance_1 = SubjectInfo.GetComponent<InfoScript> ().maxResistance_1;
		minResistance_1 = SubjectInfo.GetComponent<InfoScript> ().minResistance_1;
		maxResistance_2 = SubjectInfo.GetComponent<InfoScript> ().maxResistance_2;
		minResistance_2 = SubjectInfo.GetComponent<InfoScript> ().minResistance_2;

	
	}
	
	// Update is called once per frame
	void Update () {
		getResistanceFromMenu ();

	
	}


	public void next(){
		if (phase == 0) {
			InstructionText.text = "Please flex your joint 1 as much as possible"; //phase 1 text
			zeroButton.SetActive(false);
			startCalibration.gameObject.SetActive (true);
			phase = 1;
		}
		else if (phase == 1) {
			InstructionText.text = "Please extend your joint 1 as much as possible"; //phase 2 text
			phase = 2;
			//prevButton.SetActive (true);
		} else if (phase == 2) {
			InstructionText.text = "Please flex your joint 2 as much as possible"; //phase 3 text
			phase = 3;
		} else if (phase == 3) {
			InstructionText.text = "Please extend your joint 2 as much as possible"; //phase 4 text
			//nextText.text = "Done";
			phase = 4;
			nextButton.SetActive (false);
		} else if (phase == 4) {
			goBack ();
		}
	}

	public void previous(){
		if (phase == 4) {
			InstructionText.text = "Please flex your joint 2 as much as possible"; //phase 3 text
			phase = 3;
			nextButton.SetActive (true);
		} else if (phase == 3) {
			InstructionText.text = "Please extend your joint 1 as much as possible"; //phase 2 text
			phase = 2;
		} else if (phase == 2) {
			InstructionText.text = "Please flex your joint 1 as much as possible";
			phase = 1;
			//prevButton.SetActive (false);
		} else if (phase == 1) {
			phase = 0;
			InstructionText.text = "Please stand upright";
			startCalibration.gameObject.SetActive (false);
			zeroButton.SetActive (true);
		}
	}

	//EFFECTS: Clears everything from the game calibration scene and calls the LoadMenu function
	//         from MenuGameCalScript
	public void goBack(){
		GameObject Camera = GameObject.Find ("Main Camera");
		GameObject Light = GameObject.Find ("Directional Light");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject EventSystem = GameObject.Find ("EventSystem");

		Destroy (Camera);
		Destroy (Light);
		Destroy (Canvas);
		Destroy (EventSystem);

		MenuGameCalObject.GetComponent<MenuGameCalScript> ().LoadMenu ();


	}


	public void testOnValueChanged(string val){
		UnityEngine.Debug.Log ("OnValueChanged called, the string received is "+val);
	}

	public void testOnEndEdit(string val){
		UnityEngine.Debug.Log ("OnEndEdit, the string received is "+val);
	}



	/// <summary>
	/// EFFECTS: Records the value for the maximum flexion and extension for each joint and calculates the
	/// scales for board to use for moving the board using sensor data.  This is called when the Calibrate button is clicked
	/// and can be called as many times as the user wants by clicking the Calibrate button in one particular phase
	/// Zero position in terms of the postio of the board is midway between minResistance and maxResistance
	/// maxAngle is the maximum angular displacement of the board
	/// 2*(resistance-zeroPos)/(maxResitance-minResitance) = angDisp/maxAngle;
	/// angDisp = 2*maxAngle*(resistance-zeroPos)/(maxResistance-minResitance)
	/// Therefore, scale = 2/(maxResistance-minResistance)
	/// TEST CASES:
	/// 1) Bend both sensors to max when calibrating them max when asked to flex and bring them back to straight 
	///    when asked to extend.  The scale for each one should be close to 0.1
	/// 2) Repeat 1 but bend sensors in their opposite directions to see if sign_1 and sign_2 are negative or not
	///    tempScale_1 and tempScale_2 must be negative as well
	/// 3) Bend one sensor positively and the other negatively
	/// 4) The joint angle should have a positive value whenever flexing and negative whenever extending 
	///    regardless of which direction the sensor had to be bent in in order to perform these actions.
	/// 5)  In each case. check sign_1 and sign_2 in both InfoScript and ArdController
	/// LATER MODIFICATIONS: Send anatomical calibration data to log after the attempt by ArdController
	/// 
	///NOTE: It must be noted that computeSensorDisplacements() and computeJointAngles() functions must be called in order 
	///to compute the necessary values, they won't be called by themselves since the board is deactivated and
	///ArdController is not running
	/// </summary>
	/// 




	public void calibrate(){

		//all the necessary calibration data is stored in SubjectInfo as well so 
		//that it can be retrieved on each attempt

		//no need to call computeResitances because that is already being called from getResistanceFromMenu() function

		if (phase == 1) {



			board.GetComponent<ArdController> ().computeSensorDisplacements();
			maxResistance_1 = board.GetComponent<ArdController> ().sensorValue_1;
			SubjectInfo.GetComponent<InfoScript> ().maxResistance_1 = maxResistance_1;

			//this gets us the sign of flexion
			//this is important when computing the joint angle
			board.GetComponent<ArdController> ().sign_1 = Math.Sign (board.GetComponent<ArdController> ().sensorValue_1 - board.GetComponent<ArdController> ().anat_zero_1);
			SubjectInfo.GetComponent<InfoScript> ().sign_1 = board.GetComponent<ArdController> ().sign_1;


			board.GetComponent<ArdController>().tempScale1 = 2/(maxResistance_1 - minResistance_1);
			SubjectInfo.GetComponent<InfoScript> ().tempScale1 = board.GetComponent<ArdController> ().tempScale1;
			board.GetComponent<ArdController>().kneeZero = minResistance_1 + 0.5*(maxResistance_1-minResistance_1);
			SubjectInfo.GetComponent<InfoScript> ().kneeZero = board.GetComponent<ArdController> ().kneeZero;



			board.GetComponent<ArdController> ().Dof1_Calibration_Max = jointAngle;
			SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Max = jointAngle;
			board.GetComponent<ArdController>().m0F1 = Math.Abs((maxResistance_1-board.GetComponent<ArdController>().anat_zero_1)/jointAngle);

//			board.GetComponent<ArdController> ().computeJointAngles();
//			board.GetComponent<ArdController> ().Dof1_Calibration_Max = board.GetComponent<ArdController> ().jointAngle1;
//			SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Max = board.GetComponent<ArdController> ().Dof1_Calibration_Max;


			//next ();
//			prevButton.SetActive (true);

		}


		else if (phase == 2) {
			board.GetComponent<ArdController> ().computeSensorDisplacements();
			minResistance_1 = board.GetComponent<ArdController> ().sensorValue_1; //not actual resistance
			SubjectInfo.GetComponent<InfoScript>().minResistance_1 = minResistance_1;

			//set the scale and zeroPos for joint 1
			board.GetComponent<ArdController>().tempScale1 = 2/(maxResistance_1 - minResistance_1);
			SubjectInfo.GetComponent<InfoScript> ().tempScale1 = board.GetComponent<ArdController> ().tempScale1;
			board.GetComponent<ArdController>().kneeZero = minResistance_1 + 0.5*(maxResistance_1-minResistance_1);
			SubjectInfo.GetComponent<InfoScript> ().kneeZero = board.GetComponent<ArdController> ().kneeZero;


			//minResistance_1 is the sensor value when the calibrate button was hit
			board.GetComponent<ArdController> ().Dof1_Calibration_Min = jointAngle;
			SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Min = jointAngle;
			board.GetComponent<ArdController>().m0E1 = Math.Abs((minResistance_1-board.GetComponent<ArdController>().anat_zero_1)/jointAngle);

			if(board.GetComponent<ArdController>().m0F1==double.PositiveInfinity) board.GetComponent<ArdController>().m0F1=board.GetComponent<ArdController>().m0E1;
			if(board.GetComponent<ArdController>().m0E1==double.PositiveInfinity) board.GetComponent<ArdController>().m0E1=board.GetComponent<ArdController>().m0F1;


//			board.GetComponent<ArdController> ().computeJointAngles ();
//			board.GetComponent<ArdController> ().Dof1_Calibration_Min = board.GetComponent<ArdController> ().jointAngle1;
//			SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Min = board.GetComponent<ArdController> ().Dof1_Calibration_Min;


			//next ();

		}

		else if (phase == 3) {

			board.GetComponent<ArdController> ().computeSensorDisplacements();
			maxResistance_2 = board.GetComponent<ArdController> ().sensorValue_2;
			SubjectInfo.GetComponent<InfoScript> ().maxResistance_2 = maxResistance_2;


			board.GetComponent<ArdController>().tempScale2 = 2/(maxResistance_2 - minResistance_2);
			SubjectInfo.GetComponent<InfoScript> ().tempScale2 = board.GetComponent<ArdController> ().tempScale2;
			board.GetComponent<ArdController> ().hipZero = minResistance_2 + 0.5*(maxResistance_2-minResistance_2);
			SubjectInfo.GetComponent<InfoScript> ().hipZero = board.GetComponent<ArdController> ().hipZero;


			//this gets us the sign of flexion
			//this is important when computing the joint angle
			board.GetComponent<ArdController> ().sign_2 = Math.Sign (board.GetComponent<ArdController> ().sensorValue_2 - board.GetComponent<ArdController> ().anat_zero_2);
			SubjectInfo.GetComponent<InfoScript> ().sign_2 = board.GetComponent<ArdController> ().sign_2;


			//maxResistance_2 is the sensor value when the calibrate button was hit
			board.GetComponent<ArdController> ().Dof2_Calibration_Max = jointAngle;
			SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Max = jointAngle;
			board.GetComponent<ArdController>().m0F2 = Math.Abs((maxResistance_2-board.GetComponent<ArdController>().anat_zero_2)/jointAngle);


//			board.GetComponent<ArdController> ().computeJointAngles();
//			board.GetComponent<ArdController> ().Dof2_Calibration_Max = board.GetComponent<ArdController> ().jointAngle2;
//			SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Max = board.GetComponent<ArdController> ().Dof2_Calibration_Max;

			//next ();


		}


		else if (phase == 4) {
			board.GetComponent<ArdController> ().computeSensorDisplacements();
			minResistance_2 = board.GetComponent<ArdController> ().sensorValue_2;


			SubjectInfo.GetComponent<InfoScript> ().minResistance_2 = minResistance_2;
			//set the scale and zeroPos for joint 1
			board.GetComponent<ArdController>().tempScale2 = 2/(maxResistance_2 - minResistance_2);
			SubjectInfo.GetComponent<InfoScript> ().tempScale2 = board.GetComponent<ArdController> ().tempScale2;
			board.GetComponent<ArdController> ().hipZero = minResistance_2 + 0.5*(maxResistance_2-minResistance_2);
			SubjectInfo.GetComponent<InfoScript> ().hipZero = board.GetComponent<ArdController> ().hipZero;



			//maxResistance_2 is the sensor value when the calibrate button was hit
			board.GetComponent<ArdController> ().Dof2_Calibration_Min = jointAngle;
			SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Min = jointAngle;
			board.GetComponent<ArdController>().m0E2 = Math.Abs((minResistance_2-board.GetComponent<ArdController>().anat_zero_2)/jointAngle);

			if(board.GetComponent<ArdController>().m0F2==double.PositiveInfinity) board.GetComponent<ArdController>().m0F2=board.GetComponent<ArdController>().m0E2;
			if(board.GetComponent<ArdController>().m0E2==double.PositiveInfinity) board.GetComponent<ArdController>().m0E2=board.GetComponent<ArdController>().m0F2;



//			board.GetComponent<ArdController> ().computeJointAngles ();
//			board.GetComponent<ArdController> ().Dof2_Calibration_Min = board.GetComponent<ArdController> ().jointAngle2;
//			SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Min = board.GetComponent<ArdController> ().Dof2_Calibration_Min;


			//next ();

		}
	}


	public void onAngleEntered(string angle){
		angleWarning.gameObject.SetActive (false);
		try{
			jointAngle = Convert.ToDouble(angle);

			if(jointAngle!=0){

				if(phase==1){
					//maxResistance_1 is the sensor value when the calibrate button was hit
					board.GetComponent<ArdController> ().Dof1_Calibration_Max = jointAngle;
					SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Max = jointAngle;
					board.GetComponent<ArdController>().m0F1 = Math.Abs((maxResistance_1-board.GetComponent<ArdController>().anat_zero_1)/jointAngle);
				}

				else if(phase==2){
					//minResistance_1 is the sensor value when the calibrate button was hit
					board.GetComponent<ArdController> ().Dof1_Calibration_Min = jointAngle;
					SubjectInfo.GetComponent<InfoScript> ().Dof1_Calibration_Min = jointAngle;
					board.GetComponent<ArdController>().m0E1 = Math.Abs((minResistance_1-board.GetComponent<ArdController>().anat_zero_1)/jointAngle);
					if(board.GetComponent<ArdController>().m0F1==double.PositiveInfinity) board.GetComponent<ArdController>().m0F1=board.GetComponent<ArdController>().m0E1;
					if(board.GetComponent<ArdController>().m0E1==double.PositiveInfinity) board.GetComponent<ArdController>().m0E1=board.GetComponent<ArdController>().m0F1;

				}

				else if(phase==3){
					//maxResistance_2 is the sensor value when the calibrate button was hit
					board.GetComponent<ArdController> ().Dof2_Calibration_Max = jointAngle;
					SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Max = jointAngle;
					board.GetComponent<ArdController>().m0F2 = Math.Abs((maxResistance_2-board.GetComponent<ArdController>().anat_zero_2)/jointAngle);
				}

				else if(phase==4){
					//minResistance_2 is the sensor value when the calibrate button was hit
					board.GetComponent<ArdController> ().Dof2_Calibration_Min = jointAngle;
					SubjectInfo.GetComponent<InfoScript> ().Dof2_Calibration_Min = jointAngle;
					board.GetComponent<ArdController>().m0E2 = Math.Abs((minResistance_2-board.GetComponent<ArdController>().anat_zero_2)/jointAngle);
					if(board.GetComponent<ArdController>().m0F2==double.PositiveInfinity) board.GetComponent<ArdController>().m0F2=board.GetComponent<ArdController>().m0E2;
					if(board.GetComponent<ArdController>().m0E2==double.PositiveInfinity) board.GetComponent<ArdController>().m0E2=board.GetComponent<ArdController>().m0F2;
				}
			}




		}
		catch(FormatException e){
			angleWarning.gameObject.SetActive (true);
		}
	}


	public void zeroSensors(){
		board.GetComponent<ArdController> ().computeSensorDisplacements ();
		board.GetComponent<ArdController> ().zeroSensors ();
	}


	//EFFECTS: Calculates resitances from the menu using the menu's own serial port.  Resitances must be measured 
	//at all times for game calibration so this function must be called in update.
	//The resitances are still stored in the R1,R2,R3 & R4 variables in ArdController
	public void getResistanceFromMenu(){
		try{
			string ardString = ardPort.ReadLine ();
			string initString = "";
			//UnityEngine.Debug.Log(ardString);
			string[] voltReadings = new String[4];
			board.GetComponent<ArdController>().splitStringInto(ardString,voltReadings,4); //voltReadings now contains the different voltage readings
			//printArray(voltReadings,4);
			double V1,V2,V3,V4;
			V1 = Convert.ToDouble(voltReadings[0])*5/1023;
			V2 = Convert.ToDouble(voltReadings[1])*5/1023;
			V3 = Convert.ToDouble(voltReadings[2])*5/1023;
			V4 = Convert.ToDouble(voltReadings[3])*5/1023;



			board.GetComponent<ArdController>().computeResitances(V1,V2,V3,V4);
		}
		catch(SystemException){
		}

		UnityEngine.Debug.Log (board.GetComponent<ArdController>().R1.ToString()+" "+board.GetComponent<ArdController>().R2.ToString()+" "+board.GetComponent<ArdController>().R3.ToString()+" "+board.GetComponent<ArdController>().R4.ToString()+" ");

	}



}
