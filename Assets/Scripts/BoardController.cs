﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using UnityEngine.SceneManagement;
using System.Collections.Generic;




/*
 * http://docs.unity3d.com/ScriptReference/KeyCode.html (key codes)
 */


public class BoardController : MonoBehaviour {
	public Text countText; //has to be done with a gameobject that always stays during the game.
	public Text gameOverText;
	public float Drag=0.3f;
	public double N=3;
	bool limitReached=false;
	string playerName="Player 1";
	double size;
	GameObject cameraLookAt;
	public GameObject playAgainButton;
	public GameObject SubjectInfo;
	public Text playAgainText;
	public GameObject SetFileAgainButton;
	double transformAngleX=0;
	double transformAngleZ=0;
	public double maxAngle=22.5;
	public GameObject Player;
	GameObject MazeWalls;
	GameObject[] mazes=new GameObject[10]; //We need this kind of memory if we add a couple of new mazes 
	double tempTime;
	int i; 
	float x;
	public float t=0;
	int gameOverCount=1;
	public double voltage;
	StreamReader reader;
	public GameObject go1;
	public GameObject go2;
	public float sensitivity = 50f;
	public Vector3 sensForce = new Vector3 (0f,0f,0f);
	public bool keyBoardUse = true;
	public GameObject deactivateStore;
	delegate void del(GameObject go);
	del deactivate,activate;
	public GameObject RandomGO1,RandomGO2; //for testing purposes only 
	public int activeMaze;
	int	activeMazeAtRunTime;
	public GameObject Files;
	public GameObject ball_prefab;
	public GameObject east,west,north,south;
	public GameObject TrajectoryGenrator,PlayAgain;
	public GameObject StartingPoint;
	public int totalMazes = 6;
	public bool isRectangular = false;
	public int toggle_count = 0;
	public bool flexMode = true;
	public GameObject replayButton, MenuObject, MenuButton, stopReplayButton;
	public bool replayMode = false, startedReplaying = false;

	List<Vector2> movementHistory;
	List<Vector3> ballMovementHistory;
	Vector3 pos, curPos, lastPos;
	int histCount = 0;



	int count=0;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<GonControllerScript> ().enabled = false;

		deactivate = new del (deactivateStore.GetComponent<DeactivateScript>().deactivate);
		activate = new del (deactivateStore.GetComponent<DeactivateScript>().activate);

		GameObject MenuObject = GameObject.Find ("MenuObject");
		MenuObject.GetComponent<MenuScript> ().LoadObjects ();
		keyBoardUse = true;

		SubjectInfo = GameObject.Find ("SubjectInfo");
		maxAngle = SubjectInfo.GetComponent<InfoScript> ().maxAngle;
		Player= GameObject.Find("Player");
		gameObject.GetComponent<ArdController> ().SubjectInfo = SubjectInfo; //currently it doesn't work if these line is 
		                                                                     //not there

		int i = 1;
		System.Random r = new System.Random();
		i = r.Next (1,7); 
		cameraLookAt = GameObject.Find ("cameraLookAt");
		if (i != 7) { 
			selectMaze(i);
			activeMaze=i;
			activeMazeAtRunTime=activeMaze;
		}
			
		if (SubjectInfo.GetComponent<InfoScript> ().attempts == 0) { 
			//the below is processed if the game has been entered for the first time
			if (!SubjectInfo.GetComponent<InfoScript> ().usingDefaultSettings)
				MenuObject.GetComponent<MenuScript> ().LoadMenu ();
			//loads the menu if default settings are not being used
			else { 
				MenuObject.GetComponent<MenuScript> ().initializeSubjectFile ();
				MenuObject.GetComponent<MenuScript> ().initializeStreams ();
				MenuObject.GetComponent<MenuScript> ().menuAttemptCount = 1;
				//done to avoid errors if the menu is entered
			}
		}

		movementHistory = new List<Vector2>();
		ballMovementHistory = new List<Vector3> ();

		curPos = new Vector2 (0, 0);
		pos = new Vector2 (0, 0);
		lastPos = Vector2.zero;

		keyBoardUse = SubjectInfo.GetComponent<InfoScript> ().keyboardUse;

	
	}


	//EFFECTS: the mazes array keeps a track of all mazes so that they can be used later one
	public void selectMaze(int i){
		MazeWalls = GameObject.Find ("MazeWalls" + i.ToString ());
		if (i == 3) {
			MazeWalls.transform.localScale = new Vector3 (0.5f, 1f, 1f);

		}
		mazes [i] = MazeWalls; //Collects the active maze here
		if (MazeWalls == null)
			UnityEngine.Debug.Log ("Mazewalls not found while reset");
		if(i!=3)MazeWalls.transform.localScale = new Vector3 (1, 1, 1); //this shows up the maze when the game is played

		
		for (int j=1; j<totalMazes+1; j++) { //to avoid falling into so called fake holes.
			if (j != i) {
				GameObject problemObject = GameObject.Find ("MazeWalls" + j.ToString ()); 
				mazes[j]=problemObject; //Collects the deactivated mazes here
				problemObject.SetActive (false); 
			}
		}
	}

   
	public void changeMaze(int i){
		if (i == 3) {
			mazes[i].transform.localScale = new Vector3 (0.5f, 1f, 1f);
		}
		MazeWalls.SetActive (false);
		mazes [i].SetActive (true); //it already has a record of the maze we want to get
		MazeWalls=mazes[i];
		if(i!=3)mazes [i].transform.localScale = new Vector3 (1,1,1);
		Player.GetComponent<BallControler> ().resetPosition ();
	}


	public void replay(){
		replayMode = true;
		playAgainButton.SetActive (false);
		gameOverText.gameObject.SetActive (false);
		TrajectoryGenrator.SetActive (false);
		replayButton.SetActive (false);
		MenuButton.SetActive (false);
		stopReplayButton.SetActive (true);
		GetComponent<ArdController> ().setBoardRotation ();
		Player = SubjectInfo.GetComponent<InfoScript> ().Player;
		Player.GetComponent<BallControler> ().t = 0;
		Player.GetComponent<BallControler> ().resetPosition ();
		Player.GetComponent<Rigidbody> ().useGravity = false;
		Player.SetActive (true);
		pos = movementHistory[0];
		curPos = movementHistory[0];
		histCount++;

	}

	public void stopReplay(){
		histCount=0;
		replayMode = false;
		Player.SetActive (false);
		playAgainButton.SetActive (true);
		gameOverText.gameObject.SetActive (true);
		TrajectoryGenrator.SetActive (true);
		replayButton.SetActive (true);
		MenuButton.SetActive (true);
		stopReplayButton.SetActive (false);
	}


	// Update is called once per frame
	void Update () {
		if (!replayMode) {

			Files = GameObject.Find ("Files");
			Player = GameObject.Find ("Player");
			Vector3 vec = go2.transform.position - go1.transform.position;

			SubjectInfo = GameObject.Find ("SubjectInfo");

			if (MenuObject.GetComponent<MenuScript> ().playing) {
				movementHistory.Add (new Vector2 (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.z));
				ballMovementHistory.Add (SubjectInfo.GetComponent<InfoScript> ().Player.GetComponent<BallControler>().relPos);
			}
			//UnityEngine.Debug.Log(Player.name);

//		if (activeMazeAtRunTime != activeMaze) {
//			changeMaze(activeMaze); //change activeMaze if the maze has to be changed
//			activeMazeAtRunTime=activeMaze;
//		}


			sensForce.x = -vec.x * vec.x - vec.z * vec.z;
			sensForce.y = vec.x * vec.y;
			sensForce.z = vec.x * vec.z;


			Drag -= (float)0.1 * (float)Convert.ToDouble (Input.GetKey (KeyCode.I));
			Drag += (float)0.1 * (float)Convert.ToDouble (Input.GetKey (KeyCode.D));
			float xRotation = Drag * Input.GetAxis ("Horizontal");


			float zRotation = Drag * Input.GetAxis ("Vertical");


			float yRotation = Drag * Input.GetAxis ("Jump");
			transformAngleX += Input.GetAxis ("Vertical");
			transformAngleZ += Input.GetAxis ("Horizontal");
			//Player.transform.parent = transform;
			if (keyBoardUse)
				transform.Rotate (zRotation, yRotation, -xRotation, Space.World);
			if (Player != null)
				Player.GetComponent<BallControler> ().moveBall ();
			//Player.transform.parent = null;
			if (Player != null)
				t = Player.GetComponent<BallControler> ().t;


			if (gameOverText.text == "HIGH SCORE! \n Record Time!" && gameOverCount == 1) {
				tempTime = t;
				gameOverCount++;

		
			}
			if (gameOverText.text == "HIGH SCORE! \n Record Time!") {


			
			}

			if (Input.GetKey (KeyCode.R)) {
				SetFileAgainButton.SetActive (true);
			} 
			if (Input.GetKey (KeyCode.B)) {
				SetFileAgainButton.SetActive (false);
			} 






			if (Player != null)
				MakeAdjustments ();
			if (SubjectInfo == null)
				UnityEngine.Debug.Log ("SubjectInfo shot erased");



		} else {
			if (histCount != movementHistory.Count) {
				pos = movementHistory [histCount++];
				if (pos != curPos && !startedReplaying) {
					startedReplaying = true;
				}
				if (startedReplaying) {
					transform.Rotate (pos.x - curPos.x, 0, pos.y - curPos.y, Space.World);
					Player.transform.position = ballMovementHistory [histCount - 1].x * Vector3.Normalize (east.transform.position - west.transform.position) + ballMovementHistory [histCount - 1].y * Vector3.Normalize (south.transform.position - north.transform.position) + StartingPoint.transform.position;
				} else {
					Player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
				}
				curPos = pos;
			} else {
				histCount=0;
				replayMode = false;
				Player.SetActive (false);
				playAgainButton.SetActive (true);
				gameOverText.gameObject.SetActive (true);
				TrajectoryGenrator.SetActive (true);
				replayButton.SetActive (true);
				MenuButton.SetActive (true);
				stopReplayButton.SetActive (false);
			}
		}

	}
	void FixedUpdate () {

	}
	void lateUpdate(){


	}

	public void setCountText(float t){
		countText.text = "Time: " + t.ToString ();
	}


	//this is called when the Play Again button is clicked
	public void loadScene(){

		GameObject MenuObject = GameObject.Find ("MenuObject");
		MenuObject.GetComponent<MenuScript> ().switching_scenes = true;
		GameObject CalibrationObject = GameObject.Find ("CalibrationObject");
		Destroy (MenuObject);
		Destroy (CalibrationObject);

		playAgainText.text = "PLEASE WAIT"; //peek returns the next available character but does not consume it

		SubjectInfo.GetComponent<InfoScript> ().keyboardUse = keyBoardUse;

		SceneManager.LoadScene (1); //uncheck auto per scene in Window->Lighting to avoid darkening of scene.  'per scene' is acw
	}
	public void loadFileScene(){
		SceneManager.LoadScene (2);
	}

	//save script from destruction when scene is reloaded

	//constraints are applied in the local space of the rigidbody
	static void splitStringCSV(string input,string[] stringArr){
		int startIndex = input.IndexOf(',');
		int endIndex = input.LastIndexOf(',');
		stringArr[0] = input.Substring(0,startIndex);
		stringArr[1] = input.Substring(endIndex+1,input.Length-endIndex-1);
		
		
	}
	public void generateTrajectory(){
		PlayAgain.transform.Translate (-300,0,0);
		TrajectoryGenrator.SetActive (false);
		int trial = SubjectInfo.GetComponent<InfoScript> ().attempts;
		StreamReader readTrajectory=new StreamReader(SubjectInfo.GetComponent<InfoScript> ().folder_path+"trajectory_"+trial.ToString()+".csv");
		try{
		UnityEngine.Debug.Log (readTrajectory.ReadLine());
		gameOverText.text = "Your trajectory";

		while (readTrajectory.Peek()!=-1) {
			string position=readTrajectory.ReadLine();
			String [] numStrings2 = new String[2];
			splitStringCSV (position,numStrings2);
			float x = (float)Convert.ToDouble (numStrings2[0]),y=(float)Convert.ToDouble (numStrings2[1]);
			UnityEngine.Debug.Log (x.ToString()+", "+y.ToString());
			GameObject go;

			go=Instantiate(ball_prefab,x*Vector3.Normalize(east.transform.position-west.transform.position)+y*Vector3.Normalize(south.transform.position-north.transform.position)+StartingPoint.transform.position,transform.rotation) as GameObject;
			go.transform.parent=transform;
		}
		}
		catch(NullReferenceException e){
			gameOverText.text="A subject folder was not created!";
		}
		
	}

	void MakeAdjustments(){
		// An attempt at restricting movement
		if(transform.rotation.eulerAngles.x>maxAngle && transform.rotation.eulerAngles.x<=90){
			Time.timeScale=0f;


			if(Player!=null)Player.transform.parent=transform;  //player would fall through the floor when the adjustment would happen which is why it is temprarily made
			                                    //a child of the board

			transform.Rotate(-(transform.rotation.eulerAngles.x-(float)maxAngle),0f,0f,Space.World); //change made here
			transformAngleX=maxAngle;  //temporary stopping problem fixed
			if(Player!=null) //Player.transform.parent=null;
			Time.timeScale=1f;

		}
		if(transform.rotation.eulerAngles.x<360-maxAngle && transform.rotation.eulerAngles.x>=270 ){
			Time.timeScale=0f;
			Player.transform.parent=transform;  //player would fall through the floor when the adjustment would happen which is why it is temprarily made
			//a child of the board

			transform.Rotate(-(transform.rotation.eulerAngles.x)-(float)maxAngle+360,0f,0f,Space.World);
			transformAngleX=-maxAngle;  //temporary stopping problem fixed
			//Player.transform.parent=null;
			Time.timeScale=1f;
			
		}
		if(transform.rotation.eulerAngles.z>maxAngle && transform.rotation.eulerAngles.z<=90){

			Time.timeScale=0f;
			
			
			Player.transform.parent=transform;  //player would fall through the floor when the adjustment would happen which is why it is temprarily made
			//a child of the board
			transform.Rotate(0f,0f,-(transform.rotation.eulerAngles.z-(float)maxAngle),Space.World); //change made here
			transformAngleZ=maxAngle;  //temporary stopping problem fixed
			//Player.transform.parent=null;
			Time.timeScale=1f;
			
		}
		if(transform.rotation.eulerAngles.z<360-maxAngle && transform.rotation.eulerAngles.z>=270 ){
			//UnityEngine.Debug.Log("This is being detected");
			Time.timeScale=0f;
			Player.transform.parent=transform;  //player would fall through the floor when the adjustment would happen which is why it is temprarily made
			//a child of the board
			//UnityEngine.Debug.Log("This is being detected");
			transform.Rotate(0f,0f,(-(transform.rotation.eulerAngles.z)-(float)maxAngle+360),Space.World);
			transformAngleZ=-maxAngle;  //temporary stopping problem fixed
			//Player.transform.parent=null;
			Time.timeScale=1f;
			
		}
	}

	public void flipSensorMode(){
		flexMode = (flexMode)?false:true;
		if (flexMode) {
			gameObject.GetComponent<GonControllerScript> ().enabled = false;
			gameObject.GetComponent<ArdController> ().enabled = true;
		} else {
			gameObject.GetComponent<ArdController> ().enabled = false;
			gameObject.GetComponent<GonControllerScript> ().enabled = true;
		}
	}

	void OnEnable(){
		if (activeMazeAtRunTime != activeMaze) {
			changeMaze(activeMaze); //change activeMaze if the maze has to be changed
			activeMazeAtRunTime=activeMaze;
		}
	}


}


