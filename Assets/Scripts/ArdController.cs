﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.IO.Ports;





//Circuit plan for the sensors: Circuit for first joint 5V-front1-rear1-resistor1-GND
//                              Circuit for second joint 5V-front2-rear2-resistor2-GND


public class ArdController : MonoBehaviour {
	public double frontArd1=0,rearArd1=0,frontArd2=0,rearArd2=0; //voltage readings of the arduinos
	public double R1=10,R2=10,R3=10,R4=10;
	public double volt1=0,volt2=0, currentVolt1 = 0, currentVolt2 = 0, angDisp1 = 0, angDisp2 = 0;
	//double m1=579.710145,m2=579.710145, c=-207.768116;
	double scale1=0,scale2=0;
	string ardString;
	public SerialPort ardPort;
	public double V1=0, V2=0, V3=0, V4=0; //voltage readings after the sensors
	double angle1,angle2;
	//double R1, R2;
	double current_1,current_2,current_3,current_4;
	public double resistor_1 = 10,resistor_2 = 10,resistor_3 = 10,resistor_4 = 10; //kilohms
	double currentAngle1=0,currentAngle2=0;
	public double R1_straight = 9.67,R2_straight = 11,R3_straight = 9.34,R4_straight = 10.92; //the resistances when sensors are straight
	public StreamWriter ardPlot;
	public GameObject SubjectInfo;
	public GameObject Player;
	int calibCount = 0;
	public bool isInitialized = false; //for now, to indicate whether the setup has been initialized or not
	public double m=0.0782,c=8.9221;
	public double mForward1=0.0782,cForward1=8.9221;
	public double mRear1=0.0782,cRear1=8.9221;
	public double mForward2=0.0782,cForward2=8.9221;
	public double mRear2=0.0782,cRear2=8.9221;
	public double anat_zero_1=0, anat_zero_2=0; //the values of the resistance when the joints are in their zero 
	                                            //position
	public double sensorValue_1=0,sensorValue_2=0; //these are values assigned to the sensor bodies attached to the 
	                                               //joints
	public string portString = "/dev/cu.usbmodem1421";
	public double jointAngle1, jointAngle2;
	public double tempScale1, tempScale2;
	public double Dof1,Dof2;
	public double Dof1_Calibration_Max,Dof1_Calibration_Min, Dof2_Calibration_Max,Dof2_Calibration_Min;
	public double Dof1_Max = Double.MinValue, Dof1_Min = Double.MaxValue,Dof2_Max = Double.MinValue, Dof2_Min = Double.MaxValue;
	public double kneeZero, hipZero; //the joints need not be knee and hip
	                               //this is how these values were named in GonController
	public int sign_1 = 1,sign_2 = 1;
	double maxAngle;
	public bool flip_sensor_1 = false,flip_sensor_2 = false,flip = false;
	public Text Joint1Text, Joint2Text;
	public double m0F1 = 0.3, m0E1 = 0.3, m0F2 = 0.3, m0E2 = 0.3; 

	public bool switchOffJoint2 = false, switchOffJoint1 = false;

	//flip to be defined


	//for now all the flex sensors are assumed to be in series with each other
	//this script must be attached to the board
	//GoBack public void keyBoardToggle () function must be done again
	//InfoScript variables must be changed
	//CalibrationScript must be changed
	//Maybe set up option for changing values of the resistances in the menu
	//ACM, a new calibration scene must be prepared for the arduino
	//an option for entering the name of the serial port must be there in the menu

	// Use this for initialization
	void Start () {
		maxAngle = gameObject.GetComponent<BoardController> ().maxAngle; //in case maxAngle in changed


		SubjectInfo = GameObject.Find ("SubjectInfo");

		//this is done so that the signs can be retrieved for a new attempt
		sign_1 = SubjectInfo.GetComponent<InfoScript> ().sign_1;
		sign_2 = SubjectInfo.GetComponent<InfoScript> ().sign_2;
		tempScale1 = SubjectInfo.GetComponent<InfoScript> ().tempScale1;
		tempScale2 = SubjectInfo.GetComponent<InfoScript> ().tempScale2;
		kneeZero = SubjectInfo.GetComponent<InfoScript> ().kneeZero;
		hipZero = SubjectInfo.GetComponent<InfoScript> ().hipZero;
		m = SubjectInfo.GetComponent<InfoScript> ().m;
		c = SubjectInfo.GetComponent<InfoScript> ().m;  //in case we do on the spot calibration of the sensors
		//NOTE: This means that we would need to change the on the spot calibration scene as well
		flip = SubjectInfo.GetComponent<InfoScript>().flip;
		flip_sensor_1 = SubjectInfo.GetComponent<InfoScript> ().flip_direction_1;
		flip_sensor_2 = SubjectInfo.GetComponent<InfoScript> ().flip_direction_2;


		scale1 = gameObject.GetComponent<BoardController> ().maxAngle / 90;
		scale2 = gameObject.GetComponent<BoardController> ().maxAngle / 90;

		mForward1 = SubjectInfo.GetComponent<InfoScript> ().mForward1;
		cForward1 = SubjectInfo.GetComponent<InfoScript> ().cForward1;
		mRear1 = SubjectInfo.GetComponent<InfoScript> ().mRear1;
		cRear1 = SubjectInfo.GetComponent<InfoScript> ().cRear1;


		mForward2 = SubjectInfo.GetComponent<InfoScript> ().mForward2;
		cForward2 = SubjectInfo.GetComponent<InfoScript> ().cForward2;
		mRear2 = SubjectInfo.GetComponent<InfoScript> ().mRear2;
		cRear2 = SubjectInfo.GetComponent<InfoScript> ().cRear2;

		anat_zero_1 = SubjectInfo.GetComponent<InfoScript> ().anat_zero_1;
		anat_zero_2 = SubjectInfo.GetComponent<InfoScript> ().anat_zero_2;


		/*
		 * RESOURCE BUSY PROBLEM SOLVED: VERY IMPORTANT WHEN TESTING
		 * START ARDUINO WITHOUT STARTING GAME AND OPEN UP SERIAL MONITOR
		 * TYPE lsof | grep "/dev/cu.usbmodem1421" onto the terminal
		 * The terminal would show a process currently using the port
		 * Close the monitor and retype the command and press enter
		 * This time terminal does not show anything
		 * So whenever the game is run and the serial monitor is open, the resource gets busy
		 * Therefore make sure that the serial monitor is not being used when starting the game

		*/

		//TEMPORARY
		R1_straight = 18.8;
		R2_straight = 18.4;

	
	}
	
	// Update is called once per frame
	void Update () {

		SubjectInfo = GameObject.Find ("SubjectInfo");

		//USE getResistanceNew() FOR THE CURRENT CIRCUIT SETUP

		if (!gameObject.GetComponent<BoardController> ().keyBoardUse) {
			


			getResistancesNew();
			computeSensorDisplacements ();

			computeJointAngles ();

			Dof1_Max = (jointAngle1 > Dof1_Max) ? jointAngle1 : Dof1_Max;
			Dof1_Min = (jointAngle1 < Dof1_Min) ? jointAngle1 : Dof1_Min;
			SubjectInfo.GetComponent<InfoScript> ().Dof1_Max = Dof1_Max;
			SubjectInfo.GetComponent<InfoScript> ().Dof1_Min = Dof1_Min;

			Dof2_Max = (jointAngle2 > Dof2_Max) ? jointAngle2 : Dof2_Max;
			Dof2_Min = (jointAngle2 < Dof2_Min) ? jointAngle2 : Dof2_Min;
			SubjectInfo.GetComponent<InfoScript> ().Dof2_Max = Dof2_Max;
			SubjectInfo.GetComponent<InfoScript> ().Dof2_Min = Dof2_Min;

			//this cannot be done in the computeJointAngles() function because it is called during caibration and
			//if it is calibrating when menuGameCount==1, the loadRawData stream does not exist
			//so it would throw a NullReferenceException
			SubjectInfo.GetComponent<InfoScript> ().loadRawData.WriteLine (Player.GetComponent<BallControler>().t+", "+Dof1.ToString()+", "+Dof2.ToString()+", "+sensorValue_1.ToString()+", "+sensorValue_2.ToString());


			turnBoard ();

			try{
			ardPlot.WriteLine (Player.GetComponent<BallControler>().t.ToString()+", "+R1.ToString()+", "+R2.ToString()+", "+R3.ToString()+", "+R4.ToString());
			}
			catch(NullReferenceException e){
			}

			Joint1Text.text = "Joint1: "+Dof1.ToString ();

			Joint2Text.text = "Joint2: "+Dof2.ToString ();

		}
	
	}

	/// <summary>
	/// REQUIRES: getResistanceNEw() and computeSensorDisplacments() must be called somewhere in the update
	/// because this function is dependent on them for updating the values
	/// EFFECTS: Finds the difference between the sensor body values computed by computeSensorDisplacements() for each joint
	/// and their respective game zeros (kneeZero or hipZero) and multiplies it with the maxAngle and the particular 
	/// Which direction is flexion does not affect the direction of turning for the board since the tempScale would
	/// have the same sign which would then cancel out
	/// tempScale to scale the change in sensor values to the intended angular displacement of the board.
	/// TEST CASES:
	/// 1) Calibrate each sensor body positiviely and negatively and not the direction each time which must be the same
	///    when the sign changes
	/// 2) Go past the maximum displacement during calibration, the board must not move until the angular postion of 
	///    the sensor corresponds to an angular position of maxAngle or less in the board
	/// 3) When the sensors are straight, the board should NOT be in the zero position
	/// </summary>
	void turnBoard(){
		angle1 = sensorValue_1 - kneeZero;
		angle2 = sensorValue_2 - hipZero;

		angDisp1 = maxAngle*(angle1 - currentAngle1)*tempScale1;
		angDisp2 = maxAngle*(angle2 - currentAngle2)*tempScale2;

		currentAngle1 = angle1;
		currentAngle2 = angle2;

		if (flip_sensor_1)
			angDisp1 = -angDisp1;
		if (flip_sensor_2)
			angDisp2 = -angDisp2;

		//check if maxAngle is exceeded
		if (maxAngle * tempScale1 * Math.Abs ((sensorValue_1 - kneeZero)) > maxAngle)
			angDisp1 = 0;
		if (maxAngle * tempScale2 * Math.Abs ((sensorValue_2 - hipZero)) > maxAngle)
			angDisp2 = 0;

		if (switchOffJoint1)
			angDisp1 = 0;
		if (switchOffJoint2)
			angDisp2 = 0;

		if (flip) {
			double temp = angDisp1;
			angDisp1 = angDisp2;
			angDisp2 = temp;
		}

		transform.Rotate ((float)angDisp1,0,(float)angDisp2,Space.World);
	}

	//EFFECTS: Gets the voltage readings at the different pins and computes the resistances from them
	void getResistances(){
		try{
			ardString = ardPort.ReadLine ();
			string[] voltReadings = new String[4];
			splitStringInto(ardString,voltReadings,4); //voltReadings now contains the different voltage readings
			//printArray(voltReadings,4);
			V1 = Convert.ToDouble(voltReadings[0])*5/1023;
			V2 = Convert.ToDouble(voltReadings[1])*5/1023;
			V3 = Convert.ToDouble(voltReadings[2])*5/1023;
			V4 = Convert.ToDouble(voltReadings[3])*5/1023;
			frontArd1 = 5 - V1;
			rearArd1 = -V2 + V1;
			frontArd2 = 5-V3;
			rearArd2 = -V4 + V3;

			current_1 = V2 / resistor_1; //currents assumed always nonzero as the voltages would always be nonzero 
			current_2 = V4/ resistor_2;  //if circuit is wired correctly and does what is desired


			//The resistance of each sensor is found and stored in these variables
			if(current_1 != 0){
				R1 = frontArd1/current_1;
				R2 = rearArd1 / current_1;
			}
			if(current_2 != 0){
				R3 = frontArd2 / current_2;
				R4 = rearArd2 / current_2;
			}


		}

		catch(SystemException e){}
	}


	//EFFECTS: Finds which sensor is bending the correct way and returns c-Rrear if the rear sensor is bending 
	//and Rfront-c if the front sensor is bending.  To be used for zeroing sensors, computing joint angles and 
	//computing board angular displacements
	//ACM, any functions calling this function should be called after the getResistanceNew() function so that
	//the latest values of the resistances are used
	public void computeSensorDisplacements(){
//		sensorValue_1 = (R1>R2)?(R1-c):(-R2+c);
//		sensorValue_2 = (R3>R4)?(R3-c):(-R4+c);

		//sensorValue_1 = (R1>R2)?(R1-R1_straight):(-R2+R2_straight);
		//sensorValue_2 = (R3>R4)?(R3-R3_straight):(-R4+R4_straight);


		//ASSUMPTIONS: The other sensor does not change considerably in resistance when it is invalid
		//Straight resistance is about the same for each sensor for a particular joint
		sensorValue_1 = R1 - R2;
		sensorValue_2 = R3 - R4;

	}






	/*TEST CASES
	 1) Bend resistor 1 to full deflection and press the button, anat_zero_1 and sensorValue_1
	    should be both positive and equal to the max resistance approximately
	 2) Bend resistor 2 to full deflection and press the button, anat_zero_1 and sensorValue_1
	    should be both negative and equal to the max resistance approximately in magnitude 
	 3) Bend resistor 3 to full deflection and press the button, anat_zero_2 and sensorValue_2
	    should be both positive and equal to the max resistance approximately
	 4) Bend resistor 4 to full deflection and press the button, anat_zero_2 and sensorValue_2
	    should be both negative and equal to the max resistance approximately in magnitude 
	 
	*/
	//EFFECTS: Records sensorValue_1 and sensorValue_2 by calling computeSensorDisplacements() 
	//at the zero positions when the joints are in their zero position and stores them in 
	//anat_zero_1 and anat_zero_2.  These are used for computing anatomical joint angles
	//This should be called when the zero sensors button is pressed in the menu scene
	//The function that calls this is in GoBack.cs with the same name
	public void zeroSensors(){
		computeSensorDisplacements ();
		anat_zero_1 = sensorValue_1;
		anat_zero_2 = sensorValue_2;
		SubjectInfo.GetComponent<InfoScript> ().anat_zero_1 = anat_zero_1;
		SubjectInfo.GetComponent<InfoScript> ().anat_zero_2 = anat_zero_2;
	}


	/// <summary>
	/// EFFECTS: Computes the anatomical joint angles using sensorValue_1 and sensorValue_2
	/// TEST CASES:
	/// 1) Zero sensor body 1 at straight position and sensor body 2 at horizontal 90 degree position(backward on lego)
	/// 2) Bend sensor body 1 90 degrees tangentially, jont angle 1 should be close to 90 
	/// 3) Bend sensor body 1 in the negative direction 90 degrees tangentially, 
	///    jont angle 1 should be close to -90
	/// 4) Bring sensor body 2 in straight position, joint angle 2 should be close to -90
	/// </summary>
	public void computeJointAngles(){
		/*if (anat_zero_1 > 0) {
			if (sensorValue_1 > 0) {
				jointAngle1 = (1 / mForward1) * (sensorValue_1 - anat_zero_1)*sign_1;
			} else {
				jointAngle1 = -(1 / mForward1) * anat_zero_1 + (1 / mRear1) * sensorValue_1*sign_1;
			}
		} else {
			if (sensorValue_1 > 0) {
				jointAngle1 = -(1/mRear1)*anat_zero_1 + (1/mForward1)*sensorValue_1*sign_1;
			} else {
				jointAngle1 = (1/mRear1)*(sensorValue_1-anat_zero_1)*sign_1;
			}
		}

		if (anat_zero_2 > 0) {
			if (sensorValue_2 > 0) {
				jointAngle2 = (1 / mForward2) * (sensorValue_2 - anat_zero_2)*sign_2;
			} else {
				jointAngle2 = -(1 / mForward2) * anat_zero_2 + (1 / mRear2) * sensorValue_2*sign_2;
			}
		} else {
			if (sensorValue_2 > 0) {
				jointAngle2 = -(1/mRear2)*anat_zero_2 + (1/mForward2)*sensorValue_2*sign_2;
			} else {
				jointAngle2 = (1/mRear2)*(sensorValue_2-anat_zero_2)*sign_2;
			}
		}*/

//		jointAngle1 = (1/mForward1)*(sensorValue_1-anat_zero_1)*sign_1;
//		jointAngle2 = (1/mForward2)*(sensorValue_2-anat_zero_2)*sign_2;

		if (extension_1 ()) {
			jointAngle1 = (1 / m0F1) * (sensorValue_1 - anat_zero_1) * sign_1;
		} else {
			jointAngle1 = (1/m0E1) * (sensorValue_1 - anat_zero_1) * sign_1;
		}

		if (extension_2 ()) {
			jointAngle2 = (1 / m0F2) * (sensorValue_2 - anat_zero_2) * sign_2;
		} else {
			jointAngle2 = (1 / m0E2) * (sensorValue_2 - anat_zero_2) * sign_2;
		}

		Dof1 = jointAngle1;
		Dof2 = jointAngle2;
	}



	bool extension_1(){
		if (sign_1 * (sensorValue_1 - anat_zero_1) > 0)
			return true;
		return false;
	}

	bool extension_2(){
		if (sign_2 * (sensorValue_2 - anat_zero_2) > 0)
			return true;
		return false;
	}



	//EFFECTS: Gets the voltage readings at the different pins and computes the resistances from them
	//This assumes a circuit design of 5V-Sensor-constResistor-GND for each sensor
	public void getResistancesNew(){
		try{
			ardString = ardPort.ReadLine ();
			string initString = "";
			//The serial port sends really large values when Unity first starts the serial communication
			//this is done before the straight resistances are computed
			if(calibCount==0){
				initString = ardString;
				while(ardString==initString){
					ardString = ardPort.ReadLine ();
				}
			}
			string[] voltReadings = new String[4];
			splitStringInto(ardString,voltReadings,4); //voltReadings now contains the different voltage readings
			//printArray(voltReadings,4);
			V1 = Convert.ToDouble(voltReadings[0])*5/1023;
			V2 = Convert.ToDouble(voltReadings[1])*5/1023;
			V3 = Convert.ToDouble(voltReadings[2])*5/1023;
			V4 = Convert.ToDouble(voltReadings[3])*5/1023;


			computeResitances(V1,V2,V3,V4);

			UnityEngine.Debug.Log (R1.ToString()+" "+R2.ToString()+" "+R3.ToString()+" "+R4.ToString()+" ");




		}

		catch(SystemException e){}
	}

	public void computeResitances(double V1, double V2, double V3, double V4){
		frontArd1 = 5 - V1;
		rearArd1 = 5 - V2;
		frontArd2 = 5 - V3;
		rearArd2 = 5 - V4;

		current_1 =  V1/ resistor_1; 
		current_2 = V2/ resistor_2;  
		current_3 = V3/ resistor_3;  
		current_4 = V4/ resistor_4;  


		//The resistance of each sensor is found and stored in these variables
		if(current_1 != 0){
			R1 = frontArd1/current_1;
		}
		if(current_2 != 0){
			R2 = rearArd1 / current_2;
		}
		if(current_3 != 0){
			R3 = frontArd2 / current_3;
		}
		if(current_4 != 0){
			R4 = rearArd2 / current_4;
		}
	}


	//EFFECTS: The Arduino now sends the potential differences across the resistors instead of the voltage readungs
	//at their pins
	void getResistancesFromPotentialDifference(){
		try{
			ardString = ardPort.ReadLine ();
			//UnityEngine.Debug.Log(ardString);
			string[] voltReadings = new String[6];
			splitStringInto(ardString,voltReadings,6); //voltReadings now contains the different voltage readings
			//printArray(voltReadings,4);
			double pd1,pd2,pdConst_1,pd3,pd4,pdConst_2;

			pd1 = Convert.ToDouble(voltReadings[0])*5/1023;
			pd2 = Convert.ToDouble(voltReadings[1])*5/1023;
			pdConst_1 = Convert.ToDouble(voltReadings[2])*5/1023;
			pd3 = Convert.ToDouble(voltReadings[3])*5/1023;
			pd4 = Convert.ToDouble(voltReadings[4])*5/1023;
			pdConst_2 = Convert.ToDouble(voltReadings[5])*5/1023;


			current_1 = pdConst_1 / resistor_1; //currents assumed always nonzero as the voltages would always be nonzero 
			current_2 = pdConst_2/ resistor_2;  //if circuit is wired correctly and does what is desired


			//The resistance of each sensor is found and stored in these variables
			if(current_1 != 0){
				R1 = pd1 /current_1;
				R2 = pd2 / current_1;
			}
			if(current_2 != 0){
				R3 = pd3 / current_2;
				R4 = pd4 / current_2;
			}
			//UnityEngine.Debug.Log (R1.ToString()+" "+R2.ToString()+" "+R3.ToString()+" "+R4.ToString()+" ");



		}

		catch(SystemException e){}
		
	}

	//EFFECTS: Gets the resistances in the zero position of the joint.  For now, to be called from the keyboard toggle function in GoBack.cs
	public void getStraightResistances(){
		//getResistancesFromPotentialDifference ();
		//getResistances ();
		getResistancesNew();
		R1_straight = R1;
		R2_straight = R2;
		R3_straight = R3;
		R4_straight = R4;
		calibCount++;
		UnityEngine.Debug.Log (R1.ToString()+" "+R2.ToString()+" "+R3.ToString()+" "+R4.ToString()+" ");
	}


	//EFFECTS: Compares the resistances of the sensors with their straight resistances in order to find their bend direction
	void getAngles(){
		angle1 = (R1>=R2)?(R1-R1_straight):(-R2+R2_straight);
		angle2 = (R3>=R4)?(R3-R3_straight):(-R4+R4_straight);
	}


	//EFFECTS: We assign the higher voltage because the lower one would mean that the flex sensor is being bent in the opposite direction
	void getAngle1(){
		if ((R1-R1_straight) >= (R2-R2_straight)) {
			angle1 = 5*(R1-R1_straight); 
		} else {
			angle1 = 5*(-R2+R2_straight); 
		}
	}
	void getAngle2(){
		if ((R3-R3_straight) >= (R4-R4_straight)) {
			angle2 = 5*(R3-R3_straight); 
		} else {
			angle2 = 5*(-R4+R4_straight); 
		}
	}

	//REQUIRES: Works to the desired effects only if there are single spaces between meaningful data in the inputString
	//MODIFIES: The array readings which stores the different sections of meaningful data in the string
	//EFFECTS: At the end, the array consists of all the meaningful strings in different sections making them convenient to be used later on
	public void splitStringInto(string inputString,string[] readings, int sections){
		for(int i=0;i<sections;++i){
			//check for spaces
			int index = inputString.IndexOf(' ');
			//do the following if there are any spaces
			if(index!=-1){
				readings[i] = inputString.Substring(0,index); //assigns everything before the space to the 
				//string in the array
				//the value index is also the size of the meaningful data before the space
				//since strings are zero indexed
				inputString = inputString.Substring(index+1); //strips off everything upto 
				//and including the space

			} else { //if no space is found, readings[i] would just equal inputString
				readings[i] = inputString;
			}
		}
	}

	//EFFECTS: Restores the board to its original orientation.
	public void setBoardRotation(){
		transform.Rotate (-transform.rotation.eulerAngles.x,-transform.rotation.eulerAngles.y,-transform.rotation.eulerAngles.z,Space.World);
	}


	//EFFECTS: Starts serial communication and opens the stream for plotting the arduino data
	public void startSerialComm(){
		try{
			ardPort = new SerialPort (portString,9600);

			//ardPort.DataReceived += new SerialDataReceivedEventHandler (DataReceivedHandler);
			if (!ardPort.IsOpen) {
				ardPort.Open ();
			}
//			string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
//			string folder_path = SubjectInfo.GetComponent<InfoScript> ().folder_path;
//			//UnityEngine.Debug.Log(Path.GetFileName(Path.GetDirectoryName(folder_path)));
//			//ACM, Path.GetFileName returns blank if there is a slash at the end of the path
//			ardPlot = new StreamWriter (@folder_path+Path.GetFileName(Path.GetDirectoryName(folder_path))+"_ArduinoSignal_"+trial+".csv");
//			ardPlot.AutoFlush = true;
		}
		catch(SystemException e){}
	}
	public void initializePlotStream(){
		string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
		string folder_path = SubjectInfo.GetComponent<InfoScript> ().folder_path;
		//UnityEngine.Debug.Log(Path.GetFileName(Path.GetDirectoryName(folder_path)));
		//ACM, Path.GetFileName returns blank if there is a slash at the end of the path
		ardPlot = new StreamWriter (@folder_path+Path.GetFileName(Path.GetDirectoryName(folder_path))+"_ArduinoSignal_"+trial+".csv");
		ardPlot.AutoFlush = true;
		UnityEngine.Debug.Log ("ardPlot initialized");
	}

	void printArray (string[] arr,int size){
		string toPrint = "";
		for (int i = 0; i < size-1; ++i) {
			toPrint = toPrint + arr [i] + " ";
		}
		toPrint = toPrint + arr[size-1];
		UnityEngine.Debug.Log (toPrint);
	}




}


//			//this function finds out which sensor is being bent the wrong way 
//			getAngles();
//
//
//
//			angDisp1 = angle1 - currentAngle1;
//			angDisp2 = angle2 - currentAngle2;
//
//			currentAngle1 = angle1;
//			currentAngle2 = angle2;
//
//			//We must record all angles but if an angle yields an angular displaement pas the maximum angle, it should be
//			//taken care of
//			//We must take care of all directions, that is why Math.Abs() has been used
//			if (Math.Abs(angle1) > Math.Abs(gameObject.GetComponent<BoardController> ().maxAngle))
//				angDisp1 = 0;
//			if (Math.Abs(angle2) > Math.Abs(gameObject.GetComponent<BoardController> ().maxAngle))
//				angDisp2 = 0;
//
//			transform.Rotate ((float)angDisp1,0,(float)angDisp2,Space.World);

