﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;

///Users/vedantgupta/Desktop/Gon.txt
///Users/vedantgupta/Desktop/Roll a Ball

//All DontDestroyOnLoad objects must be destroyed before going back

public class MenuScript : MonoBehaviour {
	public bool menuScene=false;
	public bool pullUpScene = false;
	public GameObject Player;
	public GameObject board;
	public GameObject Deactivated;
	public GameObject SubjectInfo;
	GameObject[] objectsToSave;
	public GameObject Back;
	GameObject[] objects;
	Vector3 buttonPosition;
	public int toggle_count=0;
	public int menuAttemptCount=0;
	public bool playing=true; //this property is set to false by BallController when the attempt is completed
	public bool switching_scenes=false;
	string originalPath = "";

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
		menuScene = false;
		LoadObjects (); //objects are collected right at the start f everything

	
	}

	//EFFECTS: A button that needs the screen to be cleared for its function would call this function
	public void pullUp(GameObject button){
		if (!pullUpScene) {
			pullUpScene = true;
			buttonPosition=button.transform.position;
			button.transform.position = Back.transform.position;//-new Vector3(100,-100,0);
			objects = UnityEngine.Object.FindObjectsOfType<GameObject> ();
			foreach (object go in objects) {
				GameObject g = (GameObject)go;
				if (g.name != "Main Camera" && g.name != "Directional Light" && g.name != "Background" && g.name != "MenuObject" && g.name != "EventSystem" && g.name != button.name && g.name != "Canvas"
					&& g.transform.parent != button.transform) {
			
					//Objects that are still needed are not deactivated
					//these include the usual ones and in addition the button itself and its text component
				
					g.SetActive (false);

				}

			}
			button.GetComponentInChildren<Text> ().text = "Done";
		} else {
			pullUpScene=false;
			button.transform.position=buttonPosition;
			foreach (object go in objects) {
				GameObject g = (GameObject)go;

					
					
					
					g.SetActive (true);
					

				
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		Back=GameObject.Find ("Back");
	
	}

	public void changeSensitivity(float sensitivity){


		Player.GetComponent<BallControler> ().sensitivity = sensitivity*Player.GetComponent<BallControler> ().maxSensitivity;
		SubjectInfo.GetComponent<InfoScript> ().sensitivity = Player.GetComponent<BallControler> ().sensitivity;


	}

	public void changeMaxAngle(float angle){
		board.GetComponent<BoardController> ().maxAngle = angle;
		SubjectInfo.GetComponent<InfoScript> ().maxAngle = (float)board.GetComponent<BoardController> ().maxAngle;

	}


	public void LoadObjects(){
		objectsToSave = UnityEngine.Object.FindObjectsOfType<GameObject> ();
		//this captures only active objects
	}
	public void LoadMenu(){
		if (!menuScene) {
			menuScene=true;
			foreach (object go in objectsToSave) {
				GameObject g = (GameObject)go;
				if(g.name=="Board") board=g; //'Collecting' the main objects 
				if(g.name=="Player") Player=g;
				if(g.name=="Deactivated") Deactivated=g;
				if(g.name=="SubjectInfo") SubjectInfo=g;

				if(g.transform.parent==null) {g.transform.SetParent(transform);

					if(g.name!="MenuObject")g.SetActive (false);} //Do not accidentally delete gameObject!!
			}
			originalPath = SubjectInfo.GetComponent<InfoScript> ().folder_path;
			SceneManager.LoadScene(2);
			try{
			Player.GetComponent<BallControler>().t=0;
			}
			catch(MissingReferenceException e){}; //Needless here 

			SubjectInfo.GetComponent<InfoScript>().menuGameCount++; //this value keeps track of how many times the menu was accessed during the whole game
			menuAttemptCount++; //this value keeps track of how many times the menu was accessed during a particular time
			                    //this gets reset to 0 on each attempt since this is a local value of this script and not the InfoScript

			Deactivated.SetActive(true);


		}
		else if (menuScene) {
			menuScene=false;
			GameObject CalibrationObject = GameObject.Find ("CalibrationObject");
			Destroy(CalibrationObject); //because it is marked as DontDestroyOnLoad
			GameObject SensorCalibrationObject = GameObject.Find("SensorCalibrationObject");
			Destroy (SensorCalibrationObject); //because it is marked as DontDestroyOnLoad
			GameObject MenuGameCalObject = GameObject.Find("MenuGameCalObject");
			Destroy (MenuGameCalObject); //because it is marked as DontDestroyOnLoad
			GameObject SettingsObject = GameObject.Find("SettingsObject");
			Destroy (SettingsObject);
			GameObject StatsObject = GameObject.Find ("StatsObject");
			Destroy (StatsObject);
			foreach (object go in objectsToSave) {
				GameObject g = (GameObject)go;
				if(g.transform.parent==transform){
					g.SetActive (true);
				g.transform.SetParent(null);
				}
			}
			//board.GetComponent<BoardController> ().onActivated ();
			if(!playing) Player.SetActive(false); //gets activated in line 134

			//Creates the subject data file and attempt data files if the menu was entered for the first time
			//or throws away old attempt data files if the menu was accessed in the middle of an attempt
			if(playing){
			if(SubjectInfo.GetComponent<InfoScript>().menuGameCount==1){ //it must be kept in mind that this will be executed whenever
					                                                     //menuGameCount is 1
				string temp=SubjectInfo.GetComponent<InfoScript>().folder_path;
				try{
					SubjectInfo.GetComponent<InfoScript>().create_subject_directory();
					initializeSubjectFile(); //To initialize the loadData stream
				}
				catch(ArgumentException e){SubjectInfo.GetComponent<InfoScript>().folder_path=temp;SubjectInfo.GetComponent<InfoScript>().menuGameCount=0;} //default stuff goes here
				catch(UnauthorizedAccessException e){SubjectInfo.GetComponent<InfoScript>().folder_path=temp;SubjectInfo.GetComponent<InfoScript>().menuGameCount=0;} //this is a safety feature in case there are no 
					                    																															  //file names or folder names
			}
			if(menuAttemptCount==1){
				try{
						initializeStreams();

				}
				catch(ArgumentException e){}
				catch(DirectoryNotFoundException e){}
			}
			else{try{
						SubjectInfo.GetComponent<InfoScript>().loadRawData.Close();
						SubjectInfo.GetComponent<InfoScript>().loadTrajectory.Close();
						if (originalPath != SubjectInfo.GetComponent<InfoScript> ().folder_path) {
							if ( Path.GetFileName(SubjectInfo.GetComponent<InfoScript> ().folder_path)!="") {
								SubjectInfo.GetComponent<InfoScript> ().folder_path += "/";
							}
							SubjectInfo.GetComponent<InfoScript> ().folder_path += Path.GetFileName(Path.GetDirectoryName(originalPath)) + "/";
							createFolder (SubjectInfo.GetComponent<InfoScript>().folder_path);
							string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
							if (File.Exists (SubjectInfo.GetComponent<InfoScript> ().folder_path + "trajectory_" + trial + ".csv")) {
								File.Delete (SubjectInfo.GetComponent<InfoScript> ().folder_path + "trajectory_" + trial + ".csv");
							} 
							if (File.Exists (SubjectInfo.GetComponent<InfoScript> ().folder_path + "raw_data_" + trial + ".csv")) {
								File.Delete (SubjectInfo.GetComponent<InfoScript> ().folder_path + "raw_data_" + trial + ".csv");
							} 
							if (File.Exists (SubjectInfo.GetComponent<InfoScript> ().folder_path + SubjectInfo.GetComponent<InfoScript> ().Name + ".csv")) {
								File.Delete (SubjectInfo.GetComponent<InfoScript> ().folder_path + SubjectInfo.GetComponent<InfoScript> ().Name + ".csv");
							} 
							//File.Copy (originalPath + "trajectory_" + trial + ".csv", SubjectInfo.GetComponent<InfoScript> ().folder_path + "trajectory_" + trial + ".csv");
							//File.Copy (originalPath + "raw_data_" + trial + ".csv", SubjectInfo.GetComponent<InfoScript> ().folder_path + "raw_data_" + trial + ".csv");
							File.Copy (originalPath + SubjectInfo.GetComponent<InfoScript> ().Name + ".csv", SubjectInfo.GetComponent<InfoScript> ().folder_path + SubjectInfo.GetComponent<InfoScript> ().Name + ".csv");
							SubjectInfo.GetComponent<InfoScript> ().loadTrajectory.Close ();
							SubjectInfo.GetComponent<InfoScript> ().loadTrajectory = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+"trajectory_"+trial+".csv");
							SubjectInfo.GetComponent<InfoScript> ().loadRawData.Close ();
							SubjectInfo.GetComponent<InfoScript> ().loadRawData = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+"raw_data_"+trial+".csv");
							SubjectInfo.GetComponent<InfoScript> ().loadRawData.AutoFlush = true;
							SubjectInfo.GetComponent<InfoScript> ().loadData.Close ();
							SubjectInfo.GetComponent<InfoScript> ().loadData = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+SubjectInfo.GetComponent<InfoScript> ().Name+".csv", true);
							SubjectInfo.GetComponent<InfoScript> ().loadData.AutoFlush = true;
							originalPath = SubjectInfo.GetComponent<InfoScript> ().folder_path;

						}
						else initializeStreams();
				}
				catch(ArgumentException e){}
				catch(DirectoryNotFoundException e){}
					catch(NullReferenceException e){}
				Player.GetComponent<BallControler> ().resetPosition ();
				board.GetComponent<ArdController> ().setBoardRotation ();
				}


				board.GetComponent<GonControllerScript>().Dof1_Max=-180; //so that trash data is not used in order to compute these later on 
				board.GetComponent<GonControllerScript>().Dof1_Min=180;
				board.GetComponent<GonControllerScript>().Dof2_Max=-180;
				board.GetComponent<GonControllerScript>().Dof2_Min=180;
				if (!board.GetComponent<BoardController> ().keyBoardUse && !board.GetComponent<ArdController>().isInitialized) {
					//UnityEngine.D
					//Serial Communication should only be started if the keyboard mode is turned off for the first time
					//Some sort of a makeshift calibration has been done here which should also be done at the same time
					//if (SubjectInfo.GetComponent<InfoScript> ().attempts == 0) {
					board.GetComponent<ArdController>().isInitialized = true;
						board.GetComponent<ArdController> ().SubjectInfo = SubjectInfo;
						board.GetComponent<ArdController> ().startSerialComm ();
						//board.GetComponent<ArdController> ().getStraightResistances ();
					//}
					//this must be done for each attempt whener the first time the keyboard mode is turned off for that
					//particular attempt
					board.GetComponent<ArdController> ().initializePlotStream ();
				}
			}
		}



	}

	//EFFECTS: Initializes the streams loadTrajectory and loadRawData responsible for logging the data
	//         for a particular attempt
	public void initializeStreams(){
		SubjectInfo = GameObject.Find ("SubjectInfo");
		string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
		SubjectInfo.GetComponent<InfoScript> ().loadTrajectory = new StreamWriter (SubjectInfo.GetComponent<InfoScript> ().folder_path+"trajectory_"+trial.ToString()+".csv");
		SubjectInfo.GetComponent<InfoScript> ().loadTrajectory.AutoFlush = true;
		SubjectInfo.GetComponent<InfoScript> ().loadTrajectory.WriteLine ("x, y");
		SubjectInfo.GetComponent<InfoScript> ().loadRawData = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+"raw_data_"+trial+".csv",false);
		SubjectInfo.GetComponent<InfoScript> ().loadRawData.AutoFlush = true;
		SubjectInfo.GetComponent<InfoScript> ().loadRawData.Write("SubjectID, "+SubjectInfo.GetComponent<InfoScript> ().Name+"\n"+"Timestamp, "+DateTime.Now.ToString("HH:mm:ss tt")+"\nTrial, "+trial+"\n\nTime, Dof1_Anatomical, Dof2_Anatomical, Dof1_Raw, Dof2_Raw\n");
		UnityEngine.Debug.Log("Files Created");
	}

	//EFFECTS: Initializes the loadData stream responsible for logging in data for all 
	//         the attempts writing data to SubjectName.csv
	public void initializeSubjectFile(){
		SubjectInfo = GameObject.Find ("SubjectInfo");
		string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
		
		SubjectInfo.GetComponent<InfoScript> ().loadData = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+SubjectInfo.GetComponent<InfoScript> ().Name+".csv", false); //The true specifies whether to append or not.
		SubjectInfo.GetComponent<InfoScript> ().loadData.Close (); //We close because we want the actual one to append stuff
		SubjectInfo.GetComponent<InfoScript> ().loadData = new StreamWriter (@SubjectInfo.GetComponent<InfoScript> ().folder_path+SubjectInfo.GetComponent<InfoScript> ().Name+".csv", true);
		SubjectInfo.GetComponent<InfoScript> ().loadData.AutoFlush = true; //Writing to spreadsheet successful, use autoflush to force the write
		SubjectInfo.GetComponent<InfoScript> ().loadSubjectData (); //to write the first line of the file
	}
	void OnApplicationQuit(){
		try{ 
			//if menuGameCount is 1, the files and the folder has not been created yet
		if (menuScene) { //needs to work only if it is MenuScene because player is deactivated
				if(playing && SubjectInfo.GetComponent<InfoScript>().menuGameCount!=1)Player.GetComponent<BallControler>().throwAwayAttemptFiles();
				//menuGameCount = 1 would mean no files or folders have been created yet
				//so we check for menuGameCount == 1
				if(File.Exists("record.txt"))File.Delete ("record.txt"); //we have to delete this file any way
				                                                         
			}}
		catch(ArgumentException e){
		}
	}
	//Creates a directory if it does not exist and corresponding super folders
	public void createFolder(string folder_path){
		string newFolders = "";
		//newFolders = Path.GetFileName (Path.GetDirectoryName (folder_path));
		if (Path.GetFileName (folder_path) == "") {
			folder_path = Path.GetDirectoryName (folder_path);
		}
		while (!Directory.Exists(folder_path)) {
			newFolders = Path.GetFileName (folder_path) + "/" + newFolders;
			folder_path = Path.GetDirectoryName (folder_path);
		}
		Directory.CreateDirectory (folder_path + "/" + newFolders);
	}
}
