﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;



//WARNING: THIS SCRIPT IS ATTACHED TO THE SensorCalibrationObject AND NOT THE GO BACK IN THE SENSOR CALIBRATION SCENE
//THIS OBJECT IS IN THE MENUSCENE
//THE ONLY RESPONSIBILITY OF THIS OBJECT IS THE ON THE SPOT CALIBRATION OF THE SENSORS

//ACM this gameobject does not need to be destroyed since the Canvas in the menu scene is destroyed in the goBack()
//function

public class SensCalibrationButtonScript : MonoBehaviour {
	MenuControllerObject menuController;
	public GameObject MenuObject,board,Player,SubjectInfo;
	// Use this for initialization
	void Start () {
		menuController = new MenuControllerObject (gameObject,gameObject.name,3);
		MenuObject = GameObject.Find ("MenuObject");
		board = MenuObject.GetComponent<MenuScript> ().board;
		Player = MenuObject.GetComponent<MenuScript> ().Player;
		SubjectInfo = MenuObject.GetComponent<MenuScript> ().SubjectInfo;
		DontDestroyOnLoad (gameObject);//must be destroyed when going back from the parent menu scene
		//the parent menu scene is the scene where this object initially exists
		//there is no parent menu scene for the MenuObject
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadMenu(){
		menuController.LoadMenu ();
	}

	/*TEST CASES
	 * When the game has started and the menu has been accessed for the first time, i.e. menuGameCount=1
	 * (the above does not apply when playing as Foo with default settings)
	 * When menuGameCount > 1
	 * When between attempts(after having completed an attempt and before playing the next one or before exiting)
	 * In a second attempt when menuAttemptCount = 1
	 * In a second attempt when menuAttemptCount > 1
	*/
	void OnApplicationQuit(){
		try{ 
			if (menuController.menuScene) { //needs to work only if it is MenuScene because player is deactivated

				UnityEngine.Debug.Log ("SensCalibrationButtonScript OnApplicationQuit()");
				if(MenuObject.GetComponent<MenuScript>().playing && SubjectInfo.GetComponent<InfoScript>().menuGameCount!=1)Player.GetComponent<BallControler>().throwAwayAttemptFiles();
				if(File.Exists("record.txt"))File.Delete ("record.txt"); //we have to delete this file any way
			}}
		catch(ArgumentException e){
		}
	}
}
