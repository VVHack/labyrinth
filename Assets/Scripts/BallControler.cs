﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
public class BallControler : MonoBehaviour {
	private Rigidbody rb;
	Vector3 temp;
	public int frameCount=0;
	int deactivateCount;
	bool deactivated=false;
	public GameObject StartingPoint;
	GameObject board;
	double highScore;
	public Text gameOverText;
	public float t;
	bool gameover=false;
	double tempTime;
	bool highscore=false;
	bool penalising = false;
	public GameObject playAgainButton;
	public GameObject resetHighScoreButton;
	public Text ResetText;
	GameObject SubjectInfo;
	public GameObject go1;
	public GameObject go2;
	public float sensitivity = 0.000000001f;
	public float maxSensitivity=150f;
	public Vector3 sensForce = new Vector3 (0f,0f,0f);
	bool startForce = true;
	Vector3 vec;
	GameObject MenuButton;
	public GameObject deactivateStore;
	delegate void del(GameObject go);
	del deactivate,activate;
	bool playing=true;
	double displacement;
	public GameObject Tracker;
	public Text DebugText;
	public GameObject Northwall,Southwall,Eastwall,Westwall;
	public bool loadTime=false;
	public Vector3 relPos;
	GameObject MenuObject;



	public double xPos=0,yPos=0,speed=1,xVel=0,yVel=0,xAcc=0,yAcc=0;
	public Vector3 i, j; //unit vectors for the board coordinate system
	public bool translatingOnBoard = true;
	double sideOfBoard;
	public Vector3 collidingNormal = Vector3.zero;

	// Use this for initialization
	void Start () {
		deactivate = new del (deactivateStore.GetComponent<DeactivateScript>().deactivate);
		activate = new del (deactivateStore.GetComponent<DeactivateScript>().activate);
		MenuButton = GameObject.Find ("LoadMenu");
		gameOverText.text = " ";
		deactivated = false;
		rb = GetComponent<Rigidbody> ();
		StartingPoint = GameObject.Find ("StartingPoint");
		temp = new Vector3(transform.position.x,transform.position.y,transform.position.z);
		transform.position = StartingPoint.transform.position;
		board = GameObject.Find ("Board");
		board.GetComponent<BoardController> ().StartingPoint = StartingPoint;


		SubjectInfo = GameObject.Find ("SubjectInfo");
		SubjectInfo.GetComponent<InfoScript> ().Player = gameObject;


		//reads the highscore in 
//		if (SubjectInfo.GetComponent<InfoScript> ().attempts > 0) {
//
//			highScore = Convert.ToDouble (File.ReadAllText (@"record.txt"));///Users/vedantgupta/Desktop/
//
//		}
//		if (SubjectInfo.GetComponent<InfoScript> ().attempts == 0) ResetHighScore (); //to intially write to record.txt

		highScore = SubjectInfo.GetComponent<InfoScript> ().highScore;

		sensitivity = SubjectInfo.GetComponent<InfoScript> ().sensitivity;
		UnityEngine.Debug.Log (sensitivity);
		t = board.GetComponent<BoardController>().t;
		int trial = SubjectInfo.GetComponent<InfoScript> ().attempts+1;
		MenuObject = GameObject.Find ("MenuObject");

		//initializes the attempt data files if new attempt
		if (trial > 1) {
			MenuObject.GetComponent<MenuScript>().initializeStreams();
			MenuObject.GetComponent<MenuScript> ().menuAttemptCount = 1;
		}
		sideOfBoard = Eastwall.transform.position.x - Westwall.transform.position.x;
	}
	void OnCollisionEnter(Collision collision){
//		UnityEngine.Debug.Log (collision.collider.gameObject.name);
//		if (collision.collider.gameObject.name != "Board") {
//			translatingOnBoard = false;
//			UnityEngine.Debug.Log (collision.contacts [0].normal);
//			collidingNormal = collision.contacts [0].normal;
//			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
//		}
	}
	void OnCollisionExit(Collision collision){
//		if (collision.collider.gameObject.name != "Board") {
//			collidingNormal = Vector3.zero;
//			translatingOnBoard = true;
//			//gameObject.GetComponent<Rigidbody> ().isKinematic = false;
//		}
	}

	public void translateOnBoard(){
		xAcc = speed*Vector3.Dot (sensForce,i);
		yAcc = speed*Vector3.Dot (sensForce,j);
		if (Vector3.Dot (sensForce, collidingNormal) >= 0) {
			UnityEngine.Debug.Log ("Translation is happening");
			xPos += xVel;
			yPos += yVel;
			xVel += xAcc;
			yVel += yAcc;
			transform.position = (StartingPoint.transform.position + (float)xPos * i + (float)yPos * j);
		} else {
		}
	}
	
	// Update is called once per frame
	void Update () { 
		if (frameCount == 1)
			resetPosition ();
		relPos=new Vector3(Vector3.Dot(transform.position-StartingPoint.transform.position,Vector3.Normalize(Eastwall.transform.position-Westwall.transform.position)),-Vector3.Dot(transform.position-StartingPoint.transform.position,Vector3.Normalize(Northwall.transform.position-Southwall.transform.position)),0);
		//UnityEngine.Debug.Log (relPos);
		if (deactivated) {
			relPos = relPos + new Vector3 (0,-10f,0);
		}
		if (loadTime)
			UnityEngine.Debug.Log ("It is turned on");
		try{
		SubjectInfo.GetComponent<InfoScript> ().loadTrajectory.WriteLine (relPos.x.ToString()+", "+relPos.y.ToString());
		}
		catch(NullReferenceException e){
		}
		catch(ObjectDisposedException e){
		}
		SubjectInfo = GameObject.Find ("SubjectInfo");
		if (SubjectInfo == null)
			gameOverText.text = "SubjectInfo shot erased";
		else
			gameOverText.text = "";
		board = GameObject.Find ("Board");
		t += 1*Time.deltaTime ;
		board.GetComponent<BoardController> ().setCountText (t);

		Collider collider = GetComponent<SphereCollider>();
		if (frameCount - deactivateCount == 100 && deactivated==true) {

			transform.position=StartingPoint.transform.position;

			//using an empty gameobject which is a child of the board in order to locate the staring point in
			//the local coordinate system of the board an not using transform.position = temp;
			 
			//transform.localScale=new Vector3(1.0f,10.0f,1.0f); 
			//Above is an example how a game object can be resized.  
			//Don't try to do this using lossyScale because that is read only.
			//resizing parent resizes child as well so maybe the whole of the maze can resized this way

			gameObject.GetComponent<Rigidbody>().useGravity=false;
			collider.enabled = true;
			deactivated=false;
			startForce=true;
			playing = true;
			translatingOnBoard = true;

		}
		vec = go2.transform.position - go1.transform.position;
		vec = vec / vec.magnitude ;

		
//		sensForce.x = vec.x * vec.y;
//		sensForce.y = - vec.x * vec.x - vec.z*vec.z;     // +/- vec x (vec x  (0,1,0) )
//		sensForce.z = vec.y * vec.z;

		//gameObject.GetComponent<Rigidbody> ().velocity += (float)speed * sensForce;
		//UnityEngine.Debug.Log (gameObject.GetComponent<Rigidbody> ().velocity);
		//sensForce is still a unit vector

		i = (Eastwall.transform.position-Westwall.transform.position)/Vector3.Magnitude(Eastwall.transform.position-Westwall.transform.position);
		j = (Northwall.transform.position-Southwall.transform.position)/Vector3.Magnitude(Northwall.transform.position-Southwall.transform.position);
		//sensForce = Vector3(sensForce,i)
		if (startForce == true) {
			//gameObject.GetComponent<Rigidbody> ().AddForce (sensitivity * sensForce - 100f * vec); 
			//Tracker.GetComponent<Rigidbody> ().AddForce (sensitivity * sensForce - 100f * vec); 
			//In the scene, the tracker points to the Player as well
		}

		stickToBoard ();
		fly (); //just for testing purposes
		if (!isPresent ()) {
			bringBack();
		}
		frameCount++;

	
	}
	void FixedUpdate(){
		//if(translatingOnBoard) translateOnBoard ();
//		if (startForce) {
//			xAcc = speed*Vector3.Dot (sensForce,i);
//			yAcc = speed*Vector3.Dot (sensForce,j);
//			if (Vector3.Dot (sensForce, collidingNormal) >= 0) {
//				UnityEngine.Debug.Log ("Translation is happening");
//				xPos += xVel;
//				yPos += yVel;
//				xVel += xAcc;
//				yVel += yAcc;
//				transform.position = (StartingPoint.transform.position + (float)xPos * i + (float)yPos * j);
//			} 
//		}
	}
	public void ResetHighScore(){
		//File.WriteAllText (@"record.txt", (10000000). ToString ()); ///Users/vedantgupta/Desktop/
		SubjectInfo.GetComponent<InfoScript>().highScore = Double.PositiveInfinity;
		ResetText.text = "Highscore Reset";
	
	}
	void fly(){   //just for testing purposes
		if (Input.GetKey (KeyCode.F)) {
			transform.Translate(3f*vec);     
		}
	}

	public void replay(){
		gameObject.SetActive (true);
	}

	public void resetPosition(){
		transform.position=StartingPoint.transform.position;
	}

	void OnTriggerEnter(Collider other) {
		
		//UnityEngine.Debug.Log (other.gameObject.transform.parent.gameObject.name);
	  
		if (other.gameObject.CompareTag ("hole")) {
			Vector3 newVel = gameObject.GetComponent<Rigidbody> ().velocity;
			newVel = new Vector3 (0.1f*newVel.x, 0.1f*newVel.y, 0.1f*newVel.z);
			gameObject.GetComponent<Rigidbody> ().velocity = newVel;
			transform.position = other.gameObject.transform.position;
			playing=false;
			translatingOnBoard = false;
			deactivateCount=frameCount;
			Collider collider = GetComponent<SphereCollider>(); 


			collider.enabled=false;
			startForce=false;
			deactivated=true; 

			if(penalising==true)board.GetComponent<BoardController>().t+=10; //penalises by increasing the time by 10 seconds.
			                                             //this is how we access the script of a gameobject.




		}

		if (other.gameObject.CompareTag ("targetHole")) {
			playing=false;
			SubjectInfo=GameObject.Find ("SubjectInfo");
			SubjectInfo.GetComponent<InfoScript> ().attempts++;

//			SubjectInfo.GetComponent<InfoScript>().Dof1_Max=board.GetComponent<GonControllerScript>().Dof1_Max;
//			SubjectInfo.GetComponent<InfoScript>().Dof2_Max=board.GetComponent<GonControllerScript>().Dof2_Max;

			GameObject MenuObject = GameObject.Find ("MenuObject");
			try{
			SubjectInfo.GetComponent<InfoScript>().loadTrajectory.Close ();
			board.GetComponent<BoardController>().TrajectoryGenrator.SetActive(true);
				board.GetComponent<BoardController>().replayButton.SetActive(true);




			SubjectInfo.GetComponent<InfoScript>().loadAttemptData(t);
			}
			catch(NullReferenceException e){}







			Collider collider = GetComponent<SphereCollider> ();
			collider.enabled = false;
			gameObject.GetComponent<Rigidbody>().useGravity=true;
			board.GetComponent<BoardController> ().Player = gameObject;
			gameObject.SetActive(false); //this function executes and after that this script stops running as well
			gameover=true;
			tempTime=t;
			playAgainButton.SetActive(true);
			resetHighScoreButton.SetActive(true);

			SubjectInfo.GetComponent<InfoScript> ().scores.Add (t);
			
			MenuObject.GetComponent<MenuScript>().playing=false;
			if(SubjectInfo.GetComponent<InfoScript>().attempts>1){
				if(t<highScore ) {
					//File.WriteAllText("record.txt",t.ToString());///Users/vedantgupta/Desktop/
					SubjectInfo.GetComponent<InfoScript>().highScore = t;
					highscore=true;
					gameOverText.text="HIGH SCORE! \n Record Time!";


				}
				else gameOverText.text="GAME OVER \n Good Job!";

				if (t > SubjectInfo.GetComponent<InfoScript> ().lowScore) {
					SubjectInfo.GetComponent<InfoScript> ().lowScore = t;
				}
			} 
			else{
				//File.WriteAllText("record.txt",t.ToString());
				SubjectInfo.GetComponent<InfoScript>().highScore = t;
				SubjectInfo.GetComponent<InfoScript> ().lowScore = t;
				gameOverText.text="GAME OVER \n Good Job!";
			}




		}
		if (other.gameObject.CompareTag ("boardBoundary")) {
			//Debug.Log ("This does happen");
			playing=false;
			GetComponent<SphereCollider>().enabled=false;
			if(penalising==true)board.GetComponent<BoardController>().t+=20;
			//transform.position=StartingPoint.transform.position;

			deactivated=true;
			deactivateCount=frameCount;
		}
		if (!other.gameObject.CompareTag ("board")) {
			gameObject.GetComponent<Rigidbody>().useGravity=true;
		}


	}
	bool isPresent(){
		if (Mathf.Abs ((float)displacement)>5 && playing==true) {
			gameOverText.text="We'll bring it back for you";
			UnityEngine.Debug.Log("Player gone");
			resetPosition ();
			return false;
		}
		return true;
	}
	void bringBack(){
		resetPosition ();
		gameOverText.text = "";
	}
	public void stickToBoard(){
		if (playing == true) {
			if (Math.Abs (Vector3.Dot (transform.position - go1.transform.position, vec)) > 0.26) {
				Vector3 direction=transform.position - go1.transform.position;
				displacement = 0.26-Vector3.Dot (direction,vec);
				transform.Translate ((float)displacement*vec);
			}
		}
	}

	public void throwAwayAttemptFiles(){
		string trial = (SubjectInfo.GetComponent<InfoScript>().attempts+1).ToString();
		SubjectInfo.GetComponent<InfoScript> ().loadTrajectory.Close ();
		SubjectInfo.GetComponent<InfoScript> ().loadRawData.Close ();
		if (board.GetComponent<ArdController> ().ardPlot != null)
			board.GetComponent<ArdController> ().ardPlot.Close ();
		string folder_path = SubjectInfo.GetComponent<InfoScript> ().folder_path;
		//folder_path has / at the end of it, GetDirectoryName gets the whole path without / at the end
		//Then the file name is added to it so that th ename of the subject and the file name can be combined 
		//to get the final file name
		string file_path = folder_path + Path.GetFileName (Path.GetDirectoryName (folder_path)) + "_ArduinoSignal_" + trial + ".csv";
		if(File.Exists(@file_path))File.Delete (@file_path);
		File.Delete (SubjectInfo.GetComponent<InfoScript>().folder_path+"trajectory_"+trial+".csv");
		File.Delete(SubjectInfo.GetComponent<InfoScript> ().folder_path+"raw_data_"+trial+".csv");
		if (SubjectInfo.GetComponent<InfoScript> ().attempts == 0) {
			SubjectInfo.GetComponent<InfoScript> ().loadData.Close ();
			Directory.Delete (SubjectInfo.GetComponent<InfoScript> ().folder_path, true);
		}
		UnityEngine.Debug.Log ("Some files deleted");
	}
	public void moveBall(){
		sensForce.x = vec.x * vec.y;
		sensForce.y = - vec.x * vec.x - vec.z*vec.z;     // +/- vec x (vec x  (0,1,0) )
		sensForce.z = vec.y * vec.z;
		if (startForce == true) {
			gameObject.GetComponent<Rigidbody> ().AddForce (sensitivity * sensForce - 100f * vec); 
			//Tracker.GetComponent<Rigidbody> ().AddForce (sensitivity * sensForce - 100f * vec); 
			//In the scene, the tracker points to the Player as well
		}
	}

	void OnApplicationQuit(){
		try{
			UnityEngine.Debug.Log("This was called");
			throwAwayAttemptFiles();
		}
		catch(UnauthorizedAccessException e){
		}
		catch(DirectoryNotFoundException e){}
		catch(ArgumentException e){
		}
	}



}
