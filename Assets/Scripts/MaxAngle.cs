﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MaxAngle : MonoBehaviour {

	public Slider AngleSlider; //set from the game scene 
	public Text AngleText; //set from the game scene
	GameObject MenuObject;
	string initName; //refers to the text object associated with the button
	double temp;
	bool changing=false;
	string value;
	int xCorrection = 150,yCorrection = 600;
	float sizeCorrection = 0.001f;
	bool valueIsNumeric = true;
	// Use this for initialization
	void Start () {
		//initName = gameObject.GetComponentInChildren<Text>().text;
		MenuObject = GameObject.Find ("MenuObject");
		AngleSlider.value = (float)MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().maxAngle;
		temp = AngleSlider.value;
		AngleText.text = AngleSlider.value.ToString ();
		value = AngleSlider.value.ToString();
	
	}
	public void changeMaxAngle(){
		//sensitivity will be reset to the original in the next game
		if (transform.parent.gameObject.GetComponentInChildren<Text>().text == "Done") {
			//MenuObject.GetComponent<MenuScript> ().pullUp (transform.parent.gameObject);
			AngleText.gameObject.SetActive(true);
			AngleSlider.gameObject.SetActive (true);
			changing=true;
			AngleSlider.value = (float)MenuObject.GetComponent<MenuScript> ().board.GetComponent<BoardController> ().maxAngle;
			value = AngleSlider.value.ToString();
			//SensInput.gameObject.SetActive(true);
		} else {
			AngleSlider.gameObject.SetActive (false);
			AngleText.gameObject.SetActive(false);
			//gameObject.transform.parent.GetComponentInChildren<Text>().text="Game Settings";
			//MenuObject.GetComponent<MenuScript> ().pullUp (transform.parent.gameObject);
			changing=false;
			valueIsNumeric = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		AngleText.text = AngleSlider.value.ToString ();
	
	}
	void OnGUI(){
		GUI.skin.textField.fontSize = 8 * (int)Math.Ceiling(Screen.width * sizeCorrection);
		if (changing == true) {
			if(value != "") value = AngleSlider.value.ToString (); //if the user is in the process of typing in the
			//value and backspaces everything, this would not trouble the user by repeatedly setting it to the slider
			//value
			try{
				//value = (AngleSlider.value).ToString();
				//if(!value.Contains(".")) value=value+".0";
				value = (GUI.TextField (new Rect ((250+xCorrection)*Screen.width*sizeCorrection,(150+yCorrection)*Screen.height*sizeCorrection, 300*Screen.width*sizeCorrection, 30*Screen.height*sizeCorrection), value, 10000));
				double d;
				valueIsNumeric = double.TryParse(value,out d);
				//AngleSlider.value=((float)Convert.ToDouble(value));
			}
			catch(FormatException e){
				value = (AngleSlider.value).ToString();
				
			}
			if (valueIsNumeric) {
				if (Convert.ToDouble (value) > AngleSlider.maxValue) {
					value = AngleSlider.maxValue.ToString ();
				}
				AngleSlider.value = ((float)Convert.ToDouble (value));
			}


			
		}
	}
}
