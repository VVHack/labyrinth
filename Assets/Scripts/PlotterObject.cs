﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

//Plots a fixed set of points 

public class PlotterObject{
	GameObject Owner, x_axis, y_axis, Line, Point;
	GameObject scoreText;
	float min=0, max=10, xMin=0, xMax=0, yMin=0, yMax=0;
	List<float> scores = new List<float>();

	public PlotterObject(GameObject Owner, GameObject x_axis, GameObject y_axis, GameObject Line, GameObject Point, float min, float max,float xMin, float xMax,float yMin, float yMax, List<float> scores, GameObject scoreText){
		this.Owner = Owner;
		this.x_axis = x_axis;
		this.y_axis = y_axis;
		this.Line = Line;
		this.Point = Point;
		this.min = min;
		this.max = max;
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.scores = scores;
		this.scoreText = scoreText;
	}


	public void plotGraph(){

		//float xStart = x_axis.GetComponent<LineRenderer> ();

		float x1 = xMin, y1 = yMin, x2 = xMin, y2=yMin;

		int yUpper = (int)max / 10 + 1;

		int attempt = 0;



		foreach (float score in scores) {
			++attempt;


			x1 = x2;
			y1 = y2;

			x2 = xMin + ((float)attempt/(float)scores.Count)*(xMax-xMin);
			y2 = yMin + (score/(10*yUpper))*(yMax-yMin);



			GameObject line = GameObject.Instantiate (Line);
			GameObject point_1 = GameObject.Instantiate (Point);
			//GameObject point_2 = GameObject.Instantiate (Point);
			GameObject text = GameObject.Instantiate(scoreText);
			GameObject attemptLabel = GameObject.Instantiate(scoreText);
			GameObject Canvas = GameObject.Find ("Canvas");


			line.GetComponent<LineRenderer> ().material = new Material (Shader.Find("Particles/Additive"));
			line.GetComponent<LineRenderer> ().material.color = Color.blue;


			text.transform.SetParent (Canvas.transform);
			attemptLabel.transform.SetParent (Canvas.transform);
			line.transform.parent = Owner.transform;


			point_1.SetActive (true); //point_2.SetActive(true);
			line.SetActive (true);
			text.SetActive (true);
			attemptLabel.SetActive (true);


			text.GetComponent<Text> ().text = string.Format ("{0:0.00}", score) + "s"; //"Attempt " + attempt.ToString () + ", " + 
			attemptLabel.GetComponent<Text> ().text = attempt.ToString ();


			line.GetComponent<LineRenderer> ().SetPosition (0, new Vector3(x1, y1, 0));
			line.GetComponent<LineRenderer> ().SetPosition (1, new Vector3(x2, y2, 0));

			point_1.transform.position = new Vector3 (x2,y2,0);

			//GameObject Camera = GameObject.Find ("Camera");

			text.transform.position = scoreText.transform.position;
			attemptLabel.transform.position = scoreText.transform.position;


			Vector3 viewPortPoint = Camera.main.WorldToScreenPoint (new Vector2(x2, y2-0.5f));

			text.transform.position = viewPortPoint;

			text.transform.localScale = scoreText.transform.localScale;
			attemptLabel.transform.localScale = scoreText.transform.localScale;

			viewPortPoint = Camera.main.WorldToScreenPoint (new Vector2(x2+1.5f, yMin-0.5f));

			attemptLabel.transform.position = viewPortPoint;


		}

		for (int i = 0; i <= 10; ++i) {
			GameObject yText = GameObject.Instantiate(scoreText);
			GameObject Canvas = GameObject.Find ("Canvas");
			yText.transform.SetParent (Canvas.transform);
			yText.SetActive (true);
			yText.transform.localScale = scoreText.transform.localScale;
			Vector3 viewPortPoint = Camera.main.WorldToScreenPoint (new Vector2(xMin+1f, yMin+i*((yMax-yMin)/10)-0.2f));
			yText.GetComponent<Text> ().text = (i * yUpper ).ToString ();
			yText.transform.position = viewPortPoint;
		}


	}
		
}
