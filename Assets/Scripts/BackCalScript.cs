﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System;

//This is the script that actually performs the calibration, the CalibrationScript merely loads the calibration scene

public class BackCalScript : MonoBehaviour {
	GameObject CalibrationObject;
	GameObject EventSystem;
	int phase=1;
	public double maxAngle1,minAngle1,maxAngle2,minAngle2;
	public Text InstructionText;
	Stream stream;
	GameObject SubjectInfo;
	string address;
	StreamReader streamReader;
	double angle1,angle2,range1,range2;
	bool useZero=false;
	public GameObject OK,No;
	double sign_1,sign_2;

	// Use this for initialization
	void Start () {
		EventSystem = GameObject.Find ("EventSystem");
		CalibrationObject = GameObject.Find ("CalibrationObject");
		DontDestroyOnLoad (EventSystem);
		DontDestroyOnLoad (gameObject);
		DontDestroyOnLoad (GameObject.Find ("Canvas"));
		SubjectInfo=CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo;
		address = SubjectInfo.GetComponent<InfoScript> ().voltAddress;
		InstructionText.text="Use anatomical 0 for the joints?";
		OK.SetActive(true);
		No.SetActive(true);

		
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void next(){
		if (phase == 1) {
			calibrate();
			phase=2;
			InstructionText.text="Please extend your first joint as much as possible";
		}
		else if (phase == 2) {
			calibrate();
			phase=3;
			InstructionText.text="Please flex your second joint as much as possible";
		}
		else if (phase == 3) {
			calibrate();
			phase=4;
			InstructionText.text="Please extend your second joint as much as possible";
		}
		else if (phase == 4) {
			calibrate();
			InstructionText.text="You are all set";
			goBack();
		}


	}
	public void use_zero(){
		useZero = true;
		OK.SetActive (false);
		No.SetActive (false);
		InstructionText.text="Please flex your first joint as much as possible";

	}
	public void dont_use_zero(){
		useZero = false;
		OK.SetActive (false);
		No.SetActive (false);
		InstructionText.text="Please flex your first joint as much as possible";

	}
	public void calibrate(){
		stream = File.Open (@address, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); //@"C:\Users\PM&R\Desktop\data.txt"
		streamReader = new StreamReader (stream);
		string inputString = streamReader.ReadLine ();


		String [] numStrings = new String[2];
		String string1, string2;
		splitString (inputString, numStrings);
		angle1 = Convert.ToDouble (numStrings [0]);
		UnityEngine.Debug.Log (angle1 + " " +Math.Sign(angle1).ToString());
		angle2 = Convert.ToDouble (numStrings [1]);

		//this is how the angular displacement is computed:-
		//angDisp = (2*maxAngDisp/rangeOfGon) * changeInVoltage

		//all the necessary calibration data is stored in SubjectInfo as well so 
		//that it can be retrieved on each attempt

		if(phase==1){
			maxAngle1=angle1;
			sign_1=Math.Sign (angle1);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().sign_1=Math.Sign(angle1);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof1_Calibration_Max=(angle1-CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().anat_zero_1)*36*sign_1;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().Dof1_Calibration_Max=CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof1_Calibration_Max;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().sign_1=Math.Sign (angle1);
		}
		else if(phase==2){
			minAngle1=angle1;
			range1=maxAngle1-minAngle1;
			if(!useZero)CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().kneeZero=minAngle1+0.5*(maxAngle1-minAngle1);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().tempScale1 = 2/range1;
			if(!useZero)CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().kneeZero=minAngle1+0.5*(maxAngle1-minAngle1);

			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().tempScale1=2/range1;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof1_Calibration_Min=(angle1-CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().anat_zero_1)*36*sign_1;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().Dof1_Calibration_Min=CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof1_Calibration_Min;

		} //knee refers to 1 and hip refers to 2
		else if(phase==3){
			maxAngle2=angle2;
			sign_2=Math.Sign (angle2);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().sign_2=Math.Sign(angle2);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof2_Calibration_Max=(angle2-CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().anat_zero_2)*36*sign_2;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().Dof2_Calibration_Max=CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof2_Calibration_Max;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().sign_2=Math.Sign (angle2);
		}
		else if(phase==4){
			minAngle2=angle2;
			range2=maxAngle2-minAngle2;
			if(!useZero)CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().hipZero=minAngle2+0.5*(maxAngle2-minAngle2);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().tempScale2 = 2/range2;
			if(!useZero)CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().hipZero=minAngle2+0.5*(maxAngle2-minAngle2);
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().tempScale2=2/range2;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof2_Calibration_Min=(angle2-CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().anat_zero_2)*36*sign_2;
			CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().SubjectInfo.GetComponent<InfoScript>().Dof2_Calibration_Min=CalibrationObject.GetComponent<CalibrationScript>().MenuObject.GetComponent<MenuScript>().board.GetComponent<GonControllerScript>().Dof2_Calibration_Min;


		}
		
		

	}


	public void goBack(){
		GameObject camera = GameObject.Find ("Main Camera");
		GameObject light = GameObject.Find ("Directional Light");
		GameObject EventSystem = GameObject.Find ("EventSystem");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject Background = GameObject.Find ("Background");
		//Destroy (camera);
		Destroy (Background);
		Destroy (camera);
		Destroy (light);
		Destroy (EventSystem);
		Destroy (Canvas);
		CalibrationObject.GetComponent<CalibrationScript> ().LoadCalScene ();
		
		
		Destroy (gameObject);
		/*To do:-
		 * Give a variable called scene name to SubjectInfo
		 * Reset the scale whenever calibration is done or whenever max angle is changed
		 * Print the message that we assume that the goniometer to channel 1 is attached to the knee, if that is not the case, please go back to the menu and flip the goniometers
		 * The game cannot be played right away because subject info gameobject is in the initial scene
		 * Implement option for flipping sign of displacement
		 */
		
	}
	static void splitString(string input,string[] stringArr){
		//string[] stringArr=new string[1];
		int startIndex = input.IndexOf('\t');
		int endIndex = input.LastIndexOf('\t');
		stringArr[0] = input.Substring(0,startIndex);
		stringArr[1] = input.Substring(endIndex+1,input.Length-endIndex-1);
		
		
	}
}
