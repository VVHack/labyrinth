﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class goBackStats : MonoBehaviour {
	GameObject statsObject,board,SubjectInfo, MenuObject;
	public GameObject background, x_axis, y_axis, Line, Point, scoreText;
	public PlotterObject plotter;
	int count=1;

	public Text y1,y2,y3,y4,y5,y6,y7,y8,y9,y10;

	// Use this for initialization
	void Start () {
		statsObject = GameObject.Find ("StatsObject");
		board = statsObject.GetComponent<PlotStatsScript> ().board;
		SubjectInfo = statsObject.GetComponent<PlotStatsScript> ().SubjectInfo;
		MenuObject = statsObject.GetComponent<PlotStatsScript> ().MenuObject;
		float min = (float)SubjectInfo.GetComponent<InfoScript> ().highScore, max = (float)SubjectInfo.GetComponent<InfoScript> ().lowScore;
		float xMin = -7f, xMax = 6f, yMin = -3.5f, yMax = 5f;

		plotter = new PlotterObject (gameObject,  x_axis, y_axis, Line,  Point,  min,  max, xMin,  xMax, yMin, yMax, SubjectInfo.GetComponent<InfoScript>().scores, scoreText);
	}
	
	// Update is called once per frame
	void Update () {
		if (count++ == 1) {
			plotter.plotGraph ();
			//y1.text = ((int)SubjectInfo.GetComponent<InfoScript> ().lowScore / 10).ToString();
		}
	}

	public void goBack(){
		GameObject Camera = GameObject.Find ("Main Camera");
		GameObject Light = GameObject.Find ("Directional Light");
		GameObject Canvas = GameObject.Find ("Canvas");
		GameObject EventSystem = GameObject.Find ("EventSystem");
		GameObject Background = GameObject.Find ("Background");

		Destroy (Camera);
		Destroy (Light);
		Destroy (Canvas);
		Destroy (EventSystem);
		Destroy (background);

		statsObject.GetComponent<PlotStatsScript> ().LoadMenu ();
	}
}
