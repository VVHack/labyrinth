﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;

public class MenuGameCalScript : MonoBehaviour {

	/*TEST CASES
	 * 1) Test for this to be destroyed when going back from the parent scene
	 * 
	*/
	MenuControllerObject menuController;
	public GameObject MenuObject, board, Player, SubjectInfo;

	// Use this for initialization
	void Start () {
	
		menuController= new MenuControllerObject (gameObject,gameObject.name,4); //game calibration scene 
		                                                                                //is scene 5
		MenuObject = GameObject.Find ("MenuObject"); //MenuObject is present in the starter scene
		board = MenuObject.GetComponent<MenuScript>().board;
		Player = MenuObject.GetComponent<MenuScript> ().Player;
		SubjectInfo = MenuObject.GetComponent<MenuScript> ().SubjectInfo;
		DontDestroyOnLoad (gameObject); //must be destroyed when going back from the parent menu scene
		                                //the parent menu scene is the scene where this object initially exists
		                                //there is no parent menu scene for the MenuObject
		 


	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void LoadMenu(){
		menuController.LoadMenu ();
	}


	/*TEST CASES
	 * 1) Don't use default settings and go to this menu and exit, the game should log "MenuGameCalScript OnApplicationQuit()"
	 *    and not "Some files deleted" as no files have been created to be deleted
	 * 2) Use default settings and go to this menu and exit, it should log both
	 * 3) Don't use default settings and go this menu for a second time and exit, it should log both
	 * 4) In between, attempts, it should only log "MenuGameCalScript OnApplicationQuit()"
	*/
	void OnApplicationQuit(){
		try{
			if(menuController.menuScene){ //this function is to be used only if we are in the menu scene
				UnityEngine.Debug.Log ("MenuGameCalScript OnApplicationQuit()");
				if(MenuObject.GetComponent<MenuScript>().playing && SubjectInfo.GetComponent<InfoScript>().menuGameCount!=1)
					Player.GetComponent<BallControler>().throwAwayAttemptFiles();
				if(File.Exists("record.txt"))File.Delete ("record.txt"); //we have to delete this file any way
			}
		}
		catch(ArgumentException e){
		}
	}
}
