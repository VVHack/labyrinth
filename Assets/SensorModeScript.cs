﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SensorModeScript : MonoBehaviour {

	public bool flexMode = true;
	public delegate void reverseMode ();
	public Text childText;
	event reverseMode modeChanged;
	// Use this for initialization
	void Start () {
		GameObject Back = GameObject.Find ("Back");
		GameObject MenuObject = GameObject.Find ("MenuObject");
		GameObject board = MenuObject.GetComponent<MenuScript> ().board;
		if (!board.GetComponent<BoardController> ().flexMode)
			switchMode ();//check before adding any listeners to make the operation more efficient
		modeChanged += board.GetComponent<BoardController> ().flipSensorMode;
		modeChanged += Back.GetComponent<GoBack> ().flipSensorMode;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void switchMode(){
		flexMode=(flexMode)?false:true;
		if (flexMode) {
			childText.text = "Flex sensor mode";
		} else {
			childText.text = "Goniometer mode";
		}
		if(modeChanged!=null)modeChanged(); //modeChanged is null if the board's flexMode is originally tuned off when 
		//entering the menu scene
	}
}

